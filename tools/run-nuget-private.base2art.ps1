# Get the ID and security principal of the current user account
$myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()
$myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)
 
# Get the security principal for the Administrator role
$adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator
 
# Check to see if we are currently running "as Administrator"
if ($myWindowsPrincipal.IsInRole($adminRole))
{
   # We are running "as Administrator" - so change the title and background color to indicate this
   $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)"
   $Host.UI.RawUI.BackgroundColor = "DarkBlue"
   clear-host
}
else
{
   # We are not running "as Administrator" - so relaunch as administrator
   
   # Create a new process object that starts PowerShell
   $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
   
   # Specify the current script path and name as a parameter
   $newProcess.Arguments = $myInvocation.MyCommand.Definition;
   
   # Indicate that the process should be elevated
   $newProcess.Verb = "runas";
   
   # Start the new process
   [System.Diagnostics.Process]::Start($newProcess);
   
   # Exit from the current, unelevated, process
   exit
}



Add-Type -assembly "system.io.compression.filesystem"

$init_path = pwd
$curr_path = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition



cd $curr_path

.\.nuget\NuGet.Exe restore Base2art.NuGet.Server.sln

if (Test-Path "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe") {
  C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe
} else {
  C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe
}


cd $curr_path


$BackUpPath = (Get-Item ".\tools\cc-runner.zip").FullName
$Destination = (Get-Item ".\tools\").FullName
$DestinationFull = ".\tools\cc-runner"

if (-not (Test-Path $DestinationFull)) {
  [io.compression.zipfile]::ExtractToDirectory($BackUpPath, $destination)
}

if (-not (Test-Path ".\installs")) {
  mkdir ".\installs"
}

if (-not (Test-Path ".\installs\Base2art.NuGet.Server-Private\")) {

  $fullName = (Get-Item ".\src\Base2art.NuGet.Server\").FullName
  
  
  cp -Recurse -Force $fullName ".\installs"
  
  mv ".\installs\Base2art.NuGet.Server" ".\installs\Base2art.NuGet.Server-private\"
  $fullName = (Get-Item ".\installs\Base2art.NuGet.Server-private\").FullName
  
  
  $appSettingsPath = ".\installs\Base2art.NuGet.Server-private\appSettings.config"
  
  $key = "8F51FADA-D4BE-4BC6-AFEA-8B9CF8C2CC12"
  $new_key = [guid]::NewGuid().ToString()
  echo "$key $new_key"
  
  (Get-Content $appSettingsPath) | 
  Foreach-Object {$_ -replace $key,$new_key}  | 
  Out-File $appSettingsPath
}

$fullName = (Get-Item ".\installs\Base2art.NuGet.Server-Private\").FullName

cd "tools\cc-runner"
try {
  netsh http delete urlacl url=https://nuget-private.base2art.com:443/
} finally
{
}

netsh http add urlacl url=https://nuget-private.base2art.com:443/ "user=$($env:USERDOMAIN)\$($env:USERNAME)" listen=yes

.\Base2art.WebRunner.Server.exe  -r "$fullName" -v / -p 443 -h "nuget-private.base2art.com" -s

cd $init_path