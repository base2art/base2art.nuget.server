# Get the ID and security principal of the current user account
$myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()
$myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)
 
# Get the security principal for the Administrator role
$adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator
 
# Check to see if we are currently running "as Administrator"
if ($myWindowsPrincipal.IsInRole($adminRole))
{
   # We are running "as Administrator" - so change the title and background color to indicate this
   $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)"
   $Host.UI.RawUI.BackgroundColor = "DarkBlue"
   clear-host
}
else
{
   # We are not running "as Administrator" - so relaunch as administrator
   
   # Create a new process object that starts PowerShell
   $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
   
   # Specify the current script path and name as a parameter
   $newProcess.Arguments = $myInvocation.MyCommand.Definition;
   
   # Indicate that the process should be elevated
   $newProcess.Verb = "runas";
   
   # Start the new process
   [System.Diagnostics.Process]::Start($newProcess);
   
   # Exit from the current, unelevated, process
   exit
}



Add-Type -assembly "system.io.compression.filesystem"

$init_path = pwd
$curr_path = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition



cd $curr_path

.\.nuget\NuGet.Exe restore Base2art.NuGet.Server.sln

if (Test-Path "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe") {
  C:\Windows\Microsoft.NET\Framework64\v4.0.30319\msbuild.exe
} else {
  C:\Windows\Microsoft.NET\Framework\v4.0.30319\msbuild.exe
}


cd $curr_path


$BackUpPath = (Get-Item ".\tools\cc-runner.zip").FullName
$Destination = (Get-Item ".\tools\").FullName
$DestinationFull = ".\tools\cc-runner"

if (-not (Test-Path $DestinationFull)) {
  [io.compression.zipfile]::ExtractToDirectory($BackUpPath, $destination)
}

$fullName = (Get-Item ".\src\Base2art.NuGet.Server\").FullName


$appSettingsPath = ".\src\Base2art.NuGet.Server\appSettings.config"

$key = "8F51FADA-D4BE-4BC6-AFEA-8B9CF8C2CC12"
$new_key = [guid]::NewGuid().ToString()
echo "$key $new_key"

(Get-Content $appSettingsPath) | 
Foreach-Object {$_ -replace $key,$new_key}  | 
Out-File $appSettingsPath


cd "tools\cc-runner"
.\Base2art.WebRunner.Server.exe  -r "$fullName" -v / -p 7991 -h localhost

cd $init_path