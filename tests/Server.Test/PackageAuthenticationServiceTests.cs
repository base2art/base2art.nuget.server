﻿using System.Collections.Specialized;
using NUnit.Framework;

namespace Base2art.NuGetServer.Infrastructure
{
    [TestFixture]
    public class PackageAuthenticationServiceTests
    {
        [TestCase("")]
        [TestCase("foo")]
        [TestCase("not-true")]
        public void AuthenticationServiceReturnsFalseIfRequireApiKeyValueIsMalformed(string requireApiKey)
        {
            // Arrange
            var collection = new NameValueCollection();
            collection.Add("requireApiKey", requireApiKey);

            // Act
            bool result = PackageAuthenticationService.IsAuthenticatedInternal("test-apikey", collection);

            // Assert
            Assert.False(result);
        }

        [TestCase("false")]
        [TestCase("FaLse")]
        public void AuthenticationServiceReturnsTrueIfRequireApiKeyValueIsSetToFalse(string keyValue)
        {
            // Arrange
            var collection = new NameValueCollection();
            collection.Add("requireApiKey", keyValue);
            collection.Add("apiKey", "test-key");

            // Act
            bool result = PackageAuthenticationService.IsAuthenticatedInternal("incorrect-key", collection);

            // Assert
            Assert.True(result);
        }

        [TestCase(null)]
        [TestCase("incorrect-key")]
        public void AuthenticationServiceReturnsFalseIfKeyDoesNotMatchConfigurationKey(string key)
        {
            // Arrange
            var collection = new NameValueCollection();
            collection.Add("requireApiKey", "true");
            collection.Add("apiKey", "test-key");

            // Act
            bool result = PackageAuthenticationService.IsAuthenticatedInternal(key, collection);

            // Assert
            Assert.False(result);
        }

        [TestCase("test-key")]
        [TestCase("tEst-Key")]
        public void AuthenticationServiceReturnsTrueIfKeyMatchesConfigurationKey(string key)
        {
            // Arrange
            var collection = new NameValueCollection();
            collection.Add("requireApiKey", "true");
            collection.Add("apiKey", "test-key");

            // Act
            bool result = PackageAuthenticationService.IsAuthenticatedInternal(key, collection);

            // Assert
            Assert.True(result);
        }
    }
}
