﻿using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Linq;
using System.ServiceModel.Web;
using Base2art.NuGetServer.DataServices;
using NUnit.Framework;
using Moq;

namespace Base2art.NuGetServer
{
    [TestFixture]
    public class PackageSvcFacts
    {
        [Test]
        public void EnsureAllDeclaredServicesAreRegistered()
        {
            // Arrange
            var registeredServices = new List<string>();
            var config = new Mock<IDataServiceConfiguration>(MockBehavior.Strict);
            config.Setup(s => s.SetServiceOperationAccessRule(It.IsAny<string>(), ServiceOperationRights.AllRead))
                  .Callback<string, ServiceOperationRights>((svc, _) => registeredServices.Add(svc));
            var expectedServices = typeof(Packages).GetMethods()
                                                     .Where(m => m.GetCustomAttributes(inherit: false).OfType<WebGetAttribute>().Any())
                                                     .Select(m => m.Name);

            // Act
            Packages.RegisterServices(config.Object);

            // Assert
            Assert.AreEqual(expectedServices.OrderBy(s => s, StringComparer.Ordinal),
                         registeredServices.OrderBy(s => s, StringComparer.Ordinal));
        }
    }
}
