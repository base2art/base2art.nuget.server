﻿using System;
using NUnit.Framework;

namespace Base2art.NuGetServer.Infrastructure
{
    [TestFixture]
    public class HelpersTest
    {
        [Test]
        public void GetRepositoryUrlCreatesProperUrlWithRootWebApp()
        {
            // Arrange
            Uri url = new Uri("http://example.com/default.aspx");
            string applicationPath = "/";

            // Act
            string repositoryUrl = Helpers.GetRepositoryUrl(url, applicationPath);

            // Assert
            Assert.AreEqual("http://example.com/nuget", repositoryUrl);
        }

        [Test]
        public void GetRepositoryUrlCreatesProperUrlWithVirtualApp()
        {
            // Arrange
            Uri url = new Uri("http://example.com/Foo/default.aspx");
            string applicationPath = "/Foo";

            // Act
            string repositoryUrl = Helpers.GetRepositoryUrl(url, applicationPath);

            // Assert
            Assert.AreEqual("http://example.com/Foo/nuget", repositoryUrl);
        }

        [Test]
        public void GetRepositoryUrlWithNonStandardPortCreatesProperUrlWithRootWebApp()
        {
            // Arrange
            Uri url = new Uri("http://example.com:1337/default.aspx");
            string applicationPath = "/";

            // Act
            string repositoryUrl = Helpers.GetRepositoryUrl(url, applicationPath);

            // Assert
            Assert.AreEqual("http://example.com:1337/nuget", repositoryUrl);
        }

        [Test]
        public void GetRepositoryUrlWithNonStandardPortCreatesProperUrlWithVirtualApp()
        {
            // Arrange
            Uri url = new Uri("http://example.com:1337/Foo/default.aspx");
            string applicationPath = "/Foo";

            // Act
            string repositoryUrl = Helpers.GetRepositoryUrl(url, applicationPath);

            // Assert
            Assert.AreEqual("http://example.com:1337/Foo/nuget", repositoryUrl);
        }
    }
}
