namespace Base2art.NuGetServer.Infrastructure
{
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.Hosting;
    using System.Web.Routing;
    using DataServices;

    public static class PackageUtility
    {
        private static Lazy<string> packagePhysicalPath = new Lazy<string>(ResolvePackagePath);

        public static string PackagePhysicalPath
        {
            get { return packagePhysicalPath.Value; }
        }

        public static string GetPackageDownloadUri(this Package package)
        {
            var routesValues = new RouteValueDictionary
            {
                { "packageId", package.Id },
                { "version", package.Version.ToString() }
            };

            var context = HttpContext.Current;

            RouteBase route = RouteTable.Routes["DownloadPackage"];

            var vpd = route.GetVirtualPath(context.Request.RequestContext, routesValues);

            string applicationPath = Helpers.EnsureTrailingSlash(context.Request.ApplicationPath);

            return applicationPath + vpd.VirtualPath;
        }

        private static string ResolvePackagePath()
        {
            // The packagesPath could be an absolute path (rooted and use as is)
            // or a virtual path (and use as a virtual path)
            string path = ConfigurationManager.AppSettings["packagesPath"];

            if (string.IsNullOrEmpty(path))
            {
                // Default path
                return HostingEnvironment.MapPath("~/Packages");
            }

            if (path.StartsWith("~/", StringComparison.OrdinalIgnoreCase))
            {
                return HostingEnvironment.MapPath(path);
            }

            return path;
        }
    }
}
