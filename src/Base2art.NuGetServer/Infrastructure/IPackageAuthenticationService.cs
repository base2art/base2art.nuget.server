﻿namespace Base2art.NuGetServer.Infrastructure
{
    using System.Security.Principal;

    public interface IPackageAuthenticationService
    {
        bool IsAuthenticated(IPrincipal user, string apiKey, string packageId);
    }
}