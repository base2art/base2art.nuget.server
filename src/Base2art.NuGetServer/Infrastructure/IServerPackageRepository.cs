﻿namespace Base2art.NuGetServer.Infrastructure
{
    using System.Collections.Generic;
    using System.Linq;
    using DataServices;
    using NuGet;

    public interface IServerPackageRepository : IServiceBasedRepository
    {
        void RemovePackage(string packageId, SemanticVersion version);
        
        IQueryable<IPackage> Search(string searchTerm, IEnumerable<string> targetFrameworks, bool allowPrereleaseVersions, bool includeDelisted);
        
        void InvalidatePackageCache();
        
        Package GetMetadataPackage(IPackage package);
    }
}
