namespace Base2art.NuGetServer.Infrastructure
{
    using System;
    using Ninject;

    public static class NinjectBootstrapper
    {
        private static readonly Lazy<IKernel> LazyKernel = new Lazy<IKernel>(() => new StandardKernel(new Bindings()));

        public static IKernel Kernel
        {
            get
            {
                return LazyKernel.Value;
            }
        }
    }
}
