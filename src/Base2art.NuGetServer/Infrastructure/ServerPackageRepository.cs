﻿namespace Base2art.NuGetServer.Infrastructure
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.Versioning;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Configuration;
    using DataServices;
    using Newtonsoft.Json;
    using Ninject;
    using NuGet;
    using NuGet.Resources;

    /// <summary>
    /// ServerPackageRepository represents a folder of nupkgs on disk. All packages are cached during the first request in order
    /// to correctly determine attributes such as IsAbsoluteLatestVersion. Adding, removing, or making changes to packages on disk
    /// will clear the cache.
    /// </summary>
    public class ServerPackageRepository : PackageRepositoryBase, IServerPackageRepository, IPackageLookup, IDisposable
    {
        private const string NupkgHashExtension = ".hash";
        private const string NupkgTempHashExtension = ".thash";
        
        private static readonly string Filter = string.Format(CultureInfo.InvariantCulture, "*{0}", Constants.PackageExtension);
        private static readonly object PadLock = new object();
        
        private readonly IFileSystem fileSystem;
        private readonly IPackagePathResolver pathResolver;
        private readonly Func<string, bool, bool> getSetting;
        
        private FileSystemWatcher fileWatcher;
        private bool monitoringFiles = false;
        private IDictionary<IPackage, DerivedPackageData> packageCache;

        private HashSet<string> markedForObsoletion;
        
        public ServerPackageRepository(string path)
            : this(new DefaultPackagePathResolver(path), new PhysicalFileSystem(path))
        {
        }

        public ServerPackageRepository(IPackagePathResolver pathResolver, IFileSystem fileSystem, Func<string, bool, bool> getSetting = null)
        {
            if (pathResolver == null)
            {
                throw new ArgumentNullException("pathResolver");
            }

            if (fileSystem == null)
            {
                throw new ArgumentNullException("fileSystem");
            }

            this.fileSystem = fileSystem;
            this.pathResolver = pathResolver;
            this.getSetting = getSetting ?? GetBooleanAppSetting;
        }

        [Inject]
        public IHashProvider HashProvider { get; set; }

        public override string Source
        {
            get { return this.fileSystem.Root; }
        }

        public override bool SupportsPrereleasePackages
        {
            get { return true; }
        }

        private bool AllowOverrideExistingPackageOnPush
        {
            get
            {
                // If the setting is misconfigured, treat it as success (backwards compatibility).
                return this.getSetting("allowOverrideExistingPackageOnPush", true);
            }
        }

        private bool EnableDelisting
        {
            get
            {
                // If the setting is misconfigured, treat it as off (backwards compatibility).
                return this.getSetting("enableDelisting", false);
            }
        }

        private bool EnableFrameworkFiltering
        {
            get
            {
                // If the setting is misconfigured, treat it as off (backwards compatibility).
                return this.getSetting("enableFrameworkFiltering", false);
            }
        }

        private bool EnablePersistNupkgHash
        {
            get
            {
                // If the setting is misconfigured, treat it as off (backwards compatibility).
                return this.getSetting("enablePersistNupkgHash", false);
            }
        }

        /// <summary>
        /// Internal package cache containing both the packages and their metadata.
        /// This data is generated if it does not exist already.
        /// </summary>
        private IDictionary<IPackage, DerivedPackageData> PackageCache
        {
            get
            {
                lock (PadLock)
                {
                    if (this.packageCache == null)
                    {
                        if (!this.monitoringFiles)
                        {
                            // attach events the first time
                            this.monitoringFiles = true;
                            this.AttachEvents();
                        }

                        this.packageCache = this.CreateCache();
                    }

                    return this.packageCache;
                }
            }
        }

        public override IQueryable<IPackage> GetPackages()
        {
            return this.PackageCache.Keys.AsQueryable<IPackage>();
        }

        public IQueryable<Package> GetPackagesWithDerivedData()
        {
            var cache = this.PackageCache;
            return cache.Keys.Select(p => new Package(p, cache[p])).AsQueryable();
        }

        public bool Exists(string packageId, SemanticVersion version)
        {
            return this.FindPackage(packageId, version) != null;
        }

        public IPackage FindPackage(string packageId, SemanticVersion version)
        {
            return this.FindPackagesById(packageId).FirstOrDefault(p => p.Version.Equals(version));
        }

        public IEnumerable<IPackage> FindPackagesById(string packageId)
        {
            return this.GetPackages().Where(p => StringComparer.OrdinalIgnoreCase.Compare(p.Id, packageId) == 0);
        }

        /// <summary>
        /// Gives the Package containing both the IPackage and the derived metadata.
        /// The returned Package will be null if <paramref name="package" /> no longer exists in the cache.
        /// </summary>
        public Package GetMetadataPackage(IPackage package)
        {
            Package metadata = null;

            // The cache may have changed, and the metadata may no longer exist
            DerivedPackageData data = null;
            if (this.PackageCache.TryGetValue(package, out data))
            {
                metadata = new Package(package, data);
            }

            return metadata;
        }

        public IQueryable<IPackage> Search(string searchTerm, IEnumerable<string> targetFrameworks, bool allowPrereleaseVersions)
        {
            return this.Search(searchTerm, targetFrameworks, allowPrereleaseVersions, false);
        }
        
        public IQueryable<IPackage> Search(string searchTerm, IEnumerable<string> targetFrameworks, bool allowPrereleaseVersions, bool includeDelisted)
        {
            var cache = this.PackageCache;

            var packages = cache.Keys.AsQueryable()
                .Find(searchTerm)
                .FilterByPrerelease(allowPrereleaseVersions);
            
            if (!includeDelisted)
            {
                packages = packages.Where(p => p.Listed);
            }
            
            if (this.EnableFrameworkFiltering && targetFrameworks.Any())
            {
                // Get the list of framework names
                var frameworkNames = targetFrameworks.Select(frameworkName => VersionUtility.ParseFrameworkName(frameworkName));

                packages = packages.Where(package => frameworkNames.Any(frameworkName => VersionUtility.IsCompatible(frameworkName, cache[package].SupportedFrameworks)));
            }

            return packages.AsQueryable();
        }

        public IEnumerable<IPackage> GetUpdates(IEnumerable<IPackageName> packages, bool includePrerelease, bool includeAllVersions, IEnumerable<FrameworkName> targetFrameworks, IEnumerable<IVersionSpec> versionConstraints)
        {
            return this.GetUpdatesCore(packages, includePrerelease, includeAllVersions, targetFrameworks, versionConstraints);
        }

        /// <summary>
        /// Add a file to the repository.
        /// </summary>
        public override void AddPackage(IPackage package)
        {
            string fileName = this.pathResolver.GetPackageFileName(package);
            if (this.fileSystem.FileExists(fileName) && !this.AllowOverrideExistingPackageOnPush)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, NuGetResources.Error_PackageAlreadyExists, package));
            }

            lock (PadLock)
            {
                using (Stream stream = package.GetStream())
                {
                    this.fileSystem.AddFile(fileName, stream);
                }

                this.InvalidatePackageCache();
            }
        }

        /// <summary>
        /// Unlist or delete a package
        /// </summary>
        public override void RemovePackage(IPackage package)
        {
            if (package != null)
            {
                string fileName = this.pathResolver.GetPackageFileName(package);

                lock (PadLock)
                {
                    if (this.EnableDelisting)
                    {
                        var fullPath = this.fileSystem.GetFullPath(fileName);

                        if (File.Exists(fullPath))
                        {
                            // Delisted files can still be queried, therefore not deleting persisted hashes if present.
                            // Also, no need to flip hidden attribute on these since only the one from the nupkg is queried.
                            File.SetAttributes(fullPath, File.GetAttributes(fullPath) | FileAttributes.Hidden);
                        }
                        else
                        {
                            Debug.Fail("unable to find file");
                        }
                    }
                    else
                    {
                        this.fileSystem.DeleteFile(fileName);
                        if (this.EnablePersistNupkgHash)
                        {
                            this.fileSystem.DeleteFile(GetHashFile(fileName, false));
                            this.fileSystem.DeleteFile(GetHashFile(fileName, true));
                        }
                    }
                    
                    var id = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.nupkg", package.Id, package.Version);
                    
                    var item = this.markedForObsoletion;
                    if (item != null)
                    {
                        item.Add(id);
                    }
                    else
                    {
                        this.markedForObsoletion = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { id };
                    }
                    
                    this.InvalidatePackageCache();
                }
            }
        }

        /// <summary>
        /// Remove a package from the respository.
        /// </summary>
        public void RemovePackage(string packageId, SemanticVersion version)
        {
            IPackage package = this.FindPackage(packageId, version);

            this.RemovePackage(package);
        }

        /// <summary>
        /// Sets the current cache to null so it will be regenerated next time.
        /// </summary>
        public void InvalidatePackageCache()
        {
            lock (PadLock)
            {
                this.packageCache = null;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            this.DetachEvents();
        }

        private static string GetHashFile(string pathToNupkg, bool isTempFile)
        {
            // path_to_nupkg\package.nupkg => path_to_nupkg\package.hash or path_to_nupkg\package.thash
            // reason for replacing extension instead of appending: elimination potential file-system file name length limits.
            if (string.IsNullOrEmpty(pathToNupkg))
            {
                return pathToNupkg;
            }

            return Path.ChangeExtension(pathToNupkg, isTempFile ? NupkgTempHashExtension : NupkgHashExtension);
        }
        
        private static JsonConverter[] Converters()
        {
            return new JsonConverter[]
            {
                new Newtonsoft.Json.Converters.StringEnumConverter(),
                new Newtonsoft.Json.Converters.VersionConverter(),
                new CustomConverter<FrameworkAssemblyReference>(() => new FrameworkAssemblyReference("Unknown...")),
                new CustomConverter<PackageDependency>(() => new PackageDependency("Unknown...")),
                new CustomConverter<PackageDependencySet>(() => new PackageDependencySet(new FrameworkName("NoFramework", new Version()), new PackageDependency[0])),
            };
        }

        private static bool GetBooleanAppSetting(string key, bool defaultValue)
        {
            var appSettings = WebConfigurationManager.AppSettings;
            bool value;
            return !bool.TryParse(appSettings[key], out value) ? defaultValue : value;
        }

        /// <summary>
        /// *.nupkg files in the root folder
        /// </summary>
        private IEnumerable<string> GetPackageFiles()
        {
            // Check top level directory
            foreach (var path in this.fileSystem.GetFiles(string.Empty, Filter))
            {
                yield return path;
            }
        }

        private void Process(
            DiskCachePackageData cachedData,
            ConcurrentDictionary<IPackage, DerivedPackageData> packages,
            ConcurrentDictionary<string, Tuple<IPackage, DerivedPackageData>> absoluteLatest,
            ConcurrentDictionary<string, Tuple<IPackage, DerivedPackageData>> latest,
            string path,
            bool enableDelisting,
            bool checkFrameworks)
        {
            var match = cachedData.Packages.FirstOrDefault(
                x =>
                {
                    var derivedPackagePath = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.nupkg", x.Id, x.Version);
                    return string.Equals(path, derivedPackagePath, StringComparison.OrdinalIgnoreCase);
                });
            
            var zip = match ?? this.GetPackageFromSystem(path, enableDelisting);
            
            var derivedMatch = cachedData.DerivedPackages.FirstOrDefault(
                x =>
                {
                    return string.Equals(path, x.Path, StringComparison.OrdinalIgnoreCase);
                });
            
            var data = derivedMatch ?? this.GetDerivedPackageDataFromDisk(path);
            
            if (checkFrameworks)
            {
                data.SupportedFrameworks = zip.GetSupportedFrameworks();
            }
            
            var entry = new Tuple<IPackage, DerivedPackageData>(zip, data);
            
            // find the latest versions
            string id = zip.Id.ToUpperInvariant();
            
            // update with the highest version
            absoluteLatest.AddOrUpdate(id, entry, (oldId, oldEntry) => oldEntry.Item1.Version < entry.Item1.Version ? entry : oldEntry);
            
            // update latest for release versions
            if (zip.IsReleaseVersion())
            {
                latest.AddOrUpdate(id, entry, (oldId, oldEntry) => oldEntry.Item1.Version < entry.Item1.Version ? entry : oldEntry);
            }
            
            // add the package to the cache, it should not exist already
            Debug.Assert(packages.ContainsKey(zip) == false, "duplicate package added");
            packages.AddOrUpdate(zip, entry.Item2, (oldPkg, oldData) => oldData);
        }

        private IPackage GetPackageFromSystem(string path, bool enableDelisting)
        {
            OptimizedZipPackage zip = this.OpenPackage(path);
            Debug.Assert(zip != null, "Unable to open " + path);
            if (zip == null)
            {
                return null;
            }
            
            if (enableDelisting)
            {
                // hidden packages are considered delisted
                zip.Listed = !File.GetAttributes(this.fileSystem.GetFullPath(path)).HasFlag(FileAttributes.Hidden);
            }
            
            return zip;
        }
        
        private DerivedPackageData GetDerivedPackageDataFromDisk(string path)
        {
            string packageHash = null;
            long packageSize = 0;
            string persistedHashFile = this.EnablePersistNupkgHash ? GetHashFile(path, false) : null;
            bool hashComputeNeeded = true;
            this.ReadHashFile(path, persistedHashFile, ref packageSize, ref packageHash, ref hashComputeNeeded);
            if (hashComputeNeeded)
            {
                this.WriteHashFile(path, persistedHashFile, packageSize, packageHash);
            }
            
            if (string.IsNullOrWhiteSpace(persistedHashFile))
            {
                using (var stream = this.fileSystem.OpenFile(path))
                {
                    packageSize = stream.Length;
                    packageHash = Convert.ToBase64String(this.HashProvider.CalculateHash(stream));
                }
            }
            
            return new DerivedPackageData
            {
                PackageSize = packageSize,
                PackageHash = packageHash,
                LastUpdated = this.fileSystem.GetLastModified(path),
                Created = this.fileSystem.GetCreated(path),
                Path = path,
                FullPath = this.fileSystem.GetFullPath(path),
                
                // default to false, these will be set later
                IsAbsoluteLatestVersion = false,
                IsLatestVersion = false
            };
        }
        
        /// <summary>
        /// CreateCache loads all packages and determines additional metadata such as the hash, IsAbsoluteLatestVersion, and IsLatestVersion.
        /// </summary>
        private IDictionary<IPackage, DerivedPackageData> CreateCache()
        {
            ConcurrentDictionary<IPackage, DerivedPackageData> packages = new ConcurrentDictionary<IPackage, DerivedPackageData>();

            ParallelOptions opts = new ParallelOptions();
            opts.MaxDegreeOfParallelism = 4;

            ConcurrentDictionary<string, Tuple<IPackage, DerivedPackageData>> absoluteLatest = new ConcurrentDictionary<string, Tuple<IPackage, DerivedPackageData>>();
            ConcurrentDictionary<string, Tuple<IPackage, DerivedPackageData>> latest = new ConcurrentDictionary<string, Tuple<IPackage, DerivedPackageData>>();

            // get settings
            bool checkFrameworks = this.EnableFrameworkFiltering;
            bool enableDelisting = this.EnableDelisting;
            
            // we need to save the current context because it's stored in TLS and we're computing hashes on different threads.
            // var context = HttpContext.Current;

            // load and cache all packages.
            // Note that we can't pass GetPackageFiles() to Parallel.ForEach() because
            // the file could be added/deleted from _fileSystem, and if this happens,
            // we'll get error "Collection was modified; enumeration operation may not execute."
            // So we have to materialize the IEnumerable into a list first.
            var packageFiles = this.GetPackageFiles().ToList();

            DiskCachePackageData diskCacheData = this.ReadData();
            
            var obsoletePackages = this.markedForObsoletion;
            this.markedForObsoletion = null;
            
            if (obsoletePackages != null)
            {
                diskCacheData.Packages =
                    diskCacheData.Packages.Where(x => !obsoletePackages.Contains(string.Format(CultureInfo.InvariantCulture, "{0}.{1}.nupkg", x.Id, x.Version))).ToArray();
                diskCacheData.DerivedPackages =
                    diskCacheData.DerivedPackages.Where(x => !obsoletePackages.Contains(x.Path)).ToArray();
            }
            
            Parallel.ForEach(
                packageFiles,
                opts,
                path => this.Process(diskCacheData, packages, absoluteLatest, latest, path, enableDelisting, checkFrameworks));

            // Set additional attributes after visiting all packages
            foreach (var entry in absoluteLatest.Values)
            {
                entry.Item2.IsAbsoluteLatestVersion = true;
            }

            foreach (var entry in latest.Values)
            {
                entry.Item2.IsLatestVersion = true;
            }
            
            diskCacheData.Packages = packages.Keys.Select(x => new SerializablePackage(x)).ToArray();
            diskCacheData.DerivedPackages = packages.Values.ToArray();
            this.WriteData(diskCacheData, true);
            
            return packages;
        }

        private void WriteHashFile(string nupkgPath, string hashFilePath, long packageSize, string packageHash)
        {
            if (hashFilePath == null)
            {
                return; // feature not enabled.
            }
            
            try
            {
                var tempHashFilePath = GetHashFile(nupkgPath, true);
                this.fileSystem.DeleteFile(tempHashFilePath);
                this.fileSystem.DeleteFile(hashFilePath);

                var content = new StringBuilder();
                content.AppendLine(packageSize.ToString(CultureInfo.InvariantCulture));
                content.AppendLine(packageHash);

                using (var stream = new MemoryStream(Encoding.ASCII.GetBytes(content.ToString())))
                {
                    this.fileSystem.AddFile(tempHashFilePath, stream);
                }
                
                // move temp file to official location when previous operation completed successfully to minimize impact of potential errors (ex: machine crash in the middle of saving the file).
                this.fileSystem.MoveFile(tempHashFilePath, hashFilePath);
            }
            catch (IOException)
            {
                // Hashing persistence is a perf optimization feature; we chose to degrade perf over degrading functionality in case of failure.
                // Log(context, string.Format(CultureInfo.InvariantCulture, "Unable to create hash file '{0}'.", hashFilePath), e);
            }
        }

        private void ReadHashFile(string nupkgPath, string hashFilePath, ref long packageSize, ref string packageHash, ref bool hashComputeNeeded)
        {
            if (hashFilePath == null)
            {
                return; // feature not enabled.
            }

            try
            {
                if (!this.fileSystem.FileExists(hashFilePath) || this.fileSystem.GetLastModified(hashFilePath) < this.fileSystem.GetLastModified(nupkgPath))
                {
                    return; // hash does not exist or is not current.
                }

                using (var stream = this.fileSystem.OpenFile(hashFilePath))
                {
                    var reader = new StreamReader(stream);
                    packageSize = long.Parse(reader.ReadLine(), CultureInfo.InvariantCulture);
                    packageHash = reader.ReadLine();
                }

                hashComputeNeeded = false;
            }
            catch (IOException)
            {
                // Hashing persistence is a perf optimization feature; we chose to degrade perf over degrading functionality in case of failure.
                // Log(context, string.Format(CultureInfo.InvariantCulture, "Unable to read hash file '{0}'.", hashFilePath), e);
            }
        }

        private OptimizedZipPackage OpenPackage(string path)
        {
            OptimizedZipPackage zip = null;

            if (this.fileSystem.FileExists(path))
            {
                try
                {
                    zip = new OptimizedZipPackage(this.fileSystem, path);
                }
                catch (FileFormatException ex)
                {
                    throw new InvalidDataException(string.Format(CultureInfo.CurrentCulture, NuGetResources.ErrorReadingPackage, path), ex);
                }
                
                // Set the last modified date on the package
                zip.Published = this.fileSystem.GetLastModified(path);
            }

            return zip;
        }

        // Add the file watcher to monitor changes on disk
        private void AttachEvents()
        {
            // skip invalid paths
            if (this.fileWatcher == null && !string.IsNullOrEmpty(this.Source) && Directory.Exists(this.Source))
            {
                this.fileWatcher = new FileSystemWatcher(this.Source);
                this.fileWatcher.Filter = Filter;
                this.fileWatcher.IncludeSubdirectories = false;

                this.fileWatcher.Changed += this.FileChanged;
                this.fileWatcher.Created += this.FileChanged;
                this.fileWatcher.Deleted += this.FileChanged;
                this.fileWatcher.Renamed += this.FileChanged;

                this.fileWatcher.EnableRaisingEvents = true;
            }
        }

        // clean up events
        private void DetachEvents()
        {
            if (this.fileWatcher != null)
            {
                this.fileWatcher.EnableRaisingEvents = false;
                this.fileWatcher.Changed -= this.FileChanged;
                this.fileWatcher.Created -= this.FileChanged;
                this.fileWatcher.Deleted -= this.FileChanged;
                this.fileWatcher.Renamed -= this.FileChanged;
                this.fileWatcher.Dispose();
                this.fileWatcher = null;
            }
        }

        private void FileChanged(object sender, FileSystemEventArgs e)
        {
            // invalidate the cache when a nupkg in the root folder changes
            // TODO: invalidating *all* packages for every nupkg change under this folder seems more expensive than it should.
            // Recommend using e.FullPath to figure out which nupkgs need to be (re)computed.
            this.InvalidatePackageCache();
        }

        private void WriteData(DiskCachePackageData data, bool overwrite)
        {
            if (overwrite || !this.fileSystem.FileExists("packages.json"))
            {
                using (var mms = new MemoryStream(System.Text.Encoding.Default.GetBytes(JsonConvert.SerializeObject(data, Formatting.Indented, Converters()))))
                {
                    mms.Seek(0L, SeekOrigin.Begin);
                    this.fileSystem.AddFile("packages.json", mms);
                }
            }
        }

        private DiskCachePackageData ReadData()
        {
            if (!this.fileSystem.FileExists("packages.json"))
            {
                this.WriteData(new DiskCachePackageData(), false);
            }
            
            string text;
            
            using (var content = this.fileSystem.OpenFile("packages.json"))
            {
                using (var sr = new StreamReader(content))
                {
                    text = sr.ReadToEnd();
                }
            }
            
            var obj = JsonConvert.DeserializeObject<DiskCachePackageData>(text, Converters());
            
            if (obj.DerivedPackages == null)
            {
                obj.DerivedPackages = new DerivedPackageData[0];
            }
            
            if (obj.Packages == null)
            {
                obj.Packages = new SerializablePackage[0];
            }
            
            return obj;
        }
        
        private class CustomConverter<T> : Newtonsoft.Json.Converters.CustomCreationConverter<T>
        {
            private readonly Func<T> obj;

            public CustomConverter(Func<T> obj)
            {
                this.obj = obj;
            }
            
            public override T Create(Type objectType)
            {
                return this.obj();
            }
        }
    }
}