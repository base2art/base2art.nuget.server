﻿namespace Base2art.NuGetServer.Infrastructure
{
    using System;
    using System.Collections.Specialized;
    using System.Security.Principal;
    using System.Web.Configuration;

    public class PackageAuthenticationService : IPackageAuthenticationService
    {
        public static bool IsAuthenticatedInternal(string apiKey, NameValueCollection appSettings)
        {
            bool value;
            if (!bool.TryParse(appSettings["requireApiKey"], out value))
            {
                // If the setting is misconfigured, fail.
                return false;
            }

            if (value == false)
            {
                // If the server's configured to allow pushing without an ApiKey, all requests are assumed to be authenticated.
                return true;
            }

            string settingsApiKey = appSettings["apiKey"];

            // No api key, no-one can push
            if (string.IsNullOrEmpty(settingsApiKey))
            {
                return false;
            }

            return string.Equals(apiKey, settingsApiKey, StringComparison.OrdinalIgnoreCase);
        }
        
        public bool IsAuthenticated(IPrincipal user, string apiKey, string packageId)
        {
            var appSettings = WebConfigurationManager.AppSettings;
            return IsAuthenticatedInternal(apiKey, appSettings);
        }
    }
}
