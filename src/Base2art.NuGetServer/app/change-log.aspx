﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Base2art.Collections.Specialized" %>

<%@ Register TagPrefix="b2a" TagName="nav" Src="~/app/nav-bar.ascx" %>
<%@ Register TagPrefix="b2a" TagName="head" Src="~/app/html-head.ascx" %>
<%@ Register TagPrefix="b2a" TagName="copyFooter" Src="~/app/body-copy-footer.ascx" %>

<script runat="server">
    
</script>


<!DOCTYPE html>
<html lang="en">
  <b2a:head runat="server" />
  <body>
    
    <b2a:nav runat="server" />
    
          
    <div class="container">
      <div>
        <h1>&nbsp;</h1>
        <!-- Example row of columns -->
        <div class="row" style="padding-top:30px;">
          <div class="col-md-4">
          
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">In Progress Items</h3>
              </div>
              <div class="panel-body">
                
                <p>
                  Look in <a href='https://trello.com/b/Shagx01H/base2art-nugetserver-inprogress'>Trello</a> to find the latest changest to the product
                </p>
                
              </div>
            </div>
          </div>
          
          
          <div class="col-md-8">
          
            <h2> Change Log </h2>
            
            <hr/>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><a href='https://trello.com/b/GhmWuiYi/base2art-nugetserver-releasedfeatures'>Version 1.0.0.1</a></h3>
              </div>
              <div class="panel-body">
                
                <ul>
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Bugs</span>
                    Columns data did not match column headings.
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Explanation about nuget</span>
                    has been added to the home page for people that drop to the site and are unclear about what nuget is.
                  </li>
                </ul>
              </div>
            </div>
            <hr/>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><a href='https://trello.com/b/GhmWuiYi/base2art-nugetserver-releasedfeatures'>Version 1.0.0.0</a></h3>
              </div>
              <div class="panel-body">
                
                <ul>
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Search</span>
                    can be used to find that one package you were looking for. Find it on the <a href='<%= VirtualPathUtility.ToAbsolute("~/app/js/vendor/bootstrap.min.js")%>'>home page</a>.
                    <p class="bg-info" style='padding:15px'>
                      I intend to make a separate page that would be used exclusively for searching and viewing packages.
                    </p>
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Basic Auth (Secured Packages)</span>
                    now ships standard with the application and is fully manageable using JSON Configuration.
                    <div class="dl-horizontal bg-info" style='padding:15px; margin-bottom:20px;'>
                      Along with being able to authenticate in the site, there is also a special way to authenticate 
                      into the NuGet server for applications (like nuget.exe) that do not support authentication.
                      You simply use a "Personal Url" which is like any other url on the site except that it has an auth token
                      embedded into the url's path.
                      <br />
                      Ex: https://nuget.example.com/auth!YWRtaW46YXNkZnF3ZXIxMjM0/nuget/
                    </div>
                    
                    <h6>Full support for the following:</h6>
                    <dl class="dl-horizontal bg-info" style='padding:15px'>
                      <dt>Users</dt>
                      <dd style='padding-bottom:17px;'>
                        Each User can have there own username and password, to which roles can be assigned.
                        <br />
                        Ex: A User (John Smith) is a user of the system.
                      </dd>
                      
                      <dt>Groups</dt>
                      <dd style='padding-bottom:17px;'>
                        A group is collection of users, to which roles can be assigned. 
                        These are best thought of as physical organization structures.
                        <br />
                        Ex: A User (Sarah Smith) is a member of the group (Developers).
                        <br />
                        Ex: A User (Merlin Catz) is a member of the group (Marketers).
                      </dd>
                      
                      <dt>Roles</dt>
                      <dd style='padding-bottom:17px;'>
                        A role acts as a tie between a user or a group, and an action or a url.
                        These are best thought of as logical organizational structures.
                        <br />
                        Ex: A User (Sarah Smith) is a characterized [role] as a 'ChangeLog:Creator'.
                        <br />
                        Ex: The Users in the Group (Marketers) are characterized [role] as a 'ChangeLog:Editor'.
                      </dd>
                      
                      <dt>Permissions</dt>
                      <dd style='padding-bottom:17px;'>
                        A permission is way to allow or deny access to url or feature based on a role, (and by proxy a user or group).
                        <br />
                        Ex: Anyone who is characterized as a 'ChangeLog:Creator' can access the page '/app/changelog.aspx'.
                        <br />
                        Ex: Anyone who is characterized as a 'ChangeLog:Editor' can press the 'save' button on '/app/changelog.aspx'.
                      </dd>
                    </dl>
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Security Management User Interface (beta)</span>
                    now ships standard with the application and can be managed through the user interface
                    <p class="bg-info" style='padding:15px'>
                      Note: While the user interface may be lacking in some cases (for-instance: pagination, of groups),
                      the enforcement of security is 100% complete and functional.  I do plan to re-do the user interface in the future
                    </p>
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Bootstrap</span>
                    is now the primary user interface framework in use.
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'><a href="https://bitbucket.org/base2art/base2art.nuget.server/src">BitBucket!</a></span>
                    is now the repository of record for this project.
                    <br/>
                    https://bitbucket.org/base2art/base2art.nuget.server/src
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Caching</span>
                    has been added so that the application fires up nearly instantaneously! it no longer takes minutes to re-start.  
                    This is done by storing the package metadata in the packages directory, 
                    as a result the application no longer has to open each file before a search can be done.
                  </li>
                  
                  
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>StyleCop &amp; FxCop Fixes</span>
                    were completed to make the code easier to maintain in the future!
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>IIS 6 and non-iis browser support</span>
                    were completed to make the code easier to test for my dev environment on a virtual machine, 
                    where a user may not have IIS
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Strong-Named</span>
                    So you can trust the quality!
                    <br />
                    DLLs are now signed with Base2art's personal code-key so you can verify the authenticity of the code 
                    I have made just for you!
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Versioning</span>
                    I have separated my software versioning from the larger NuGet project as a whole so that you can upgrade 
                    and down grade without worrying about the backing version of NuGet
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Change Logs</span>
                    are shipping with the product as you can see here.
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>Licensing</span>
                    information has been updated to indicate that I am now a primary copyright holder of this code base.
                  </li>
                  
                  <li style='padding-bottom:15px;'>
                    <span style='font-weight:bold'>BOB</span>
                    is a propietary [by Base2art] (potentially open source) build tool that makes packaging and deploying software easier.
                    I have added meta data in the source tree to manage this build process.
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <b2a:copyFooter runat='server' />
    </div> <!-- /container -->
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='<%= VirtualPathUtility.ToAbsolute("~/app/js/vendor/bootstrap.min.js")%>'></script>
    
   
  </body>
</html>


