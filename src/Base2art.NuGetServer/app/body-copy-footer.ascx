﻿
      <hr />

      <footer>
        <div class="row" style="padding-bottom:20px;">
          <div class="col-xs-6">
            
            <ul class="nav navbar-nav">
              <li style='padding-right:15px;padding-bottom:0px;'>
                &copy; Base2art <%= DateTime.Now.Year %>
              </li>
              <li>
                <a style='padding-top:0px;padding-bottom:0px;' href='<%= VirtualPathUtility.ToAbsolute("~/app/license-credit-copyright.aspx#copyright") %>'>Copyright</a>
              </li>
              <li>
                <a style='padding-top:0px;padding-bottom:0px;' href='<%= VirtualPathUtility.ToAbsolute("~/app/license-credit-copyright.aspx#credits") %>'>Credits</a>
              </li>
              <li>
                <a style='padding-top:0px;padding-bottom:0px;' href='<%= VirtualPathUtility.ToAbsolute("~/app/license-credit-copyright.aspx#license") %>'>License</a>
              </li>
            </ul>
          </div>
      </footer>