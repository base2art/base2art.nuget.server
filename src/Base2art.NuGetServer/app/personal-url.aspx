﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Base2art.Collections.Specialized" %>

<%@ Register TagPrefix="b2a" TagName="nav" Src="~/app/nav-bar.ascx" %>
<%@ Register TagPrefix="b2a" TagName="head" Src="~/app/html-head.ascx" %>
<%@ Register TagPrefix="b2a" TagName="copyFooter" Src="~/app/body-copy-footer.ascx" %>

<script runat="server">
    
    private string error;
    
    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        
        var username = this.Request.Form["__username"];
        var password = this.Request.Form["__password"];
        if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
        {
            this.error = "Invalid Value Supplied";
        }
        else
        {
            var authData = string.Concat(username, ":", password);
            authData = Base64Encode(authData);
            string url = VirtualPathUtility.ToAbsolute("~/auth!" + authData + "/nuget/");
            Uri baseUri = this.Request.Url;
            PersonalUrl.Text = new Uri(baseUri, url).ToString();
        }
    }

</script>


<!DOCTYPE html>
<html lang="en">
  <b2a:head runat="server" />
  
  <body>
  
    
    <b2a:nav runat="server" />

    
          
    <div class="container">
      <div>
        <h1>&nbsp;</h1>
        <!-- Example row of columns -->
        <div class="row" style="padding-top:30px;">
          <div class="col-md-4">
          
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Personal NuGet Url</h3>
              </div>
              <div class="panel-body">
                
                <p>
                  In order to support seemless nuget support every user gets their own Repo URL with credentials built-in.
                  Don't worry its every bit as safe as long as you use SSL!
                </p>
                
                <form method="POST" action='?'>
                  <div class="form-group">
                    <input class='form-control' name='__username' type='input' placeholder="User Name" />
                  </div>
                  <div class="form-group">
                    <input class='form-control' name='__password' type='password' placeholder="Password" />
                  </div>
                  <button type="submit" class="btn btn-default">Generate Personal Url</button>
                </form>
                
              </div>
            </div>
          </div>
          
          
          <div class="col-md-8">
          
            <h2> Generate Personal Url </h2>
            <% if (this.error != null) { %>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>ERROR!</strong> <%= this.error %>
              </div>
            <% } %>
            
            
            <asp:Literal runat="server" id="PersonalUrl" />
          </div>
        </div>
      </div>

      <b2a:copyFooter runat='server' />
    </div> <!-- /container -->
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='<%= VirtualPathUtility.ToAbsolute("~/app/js/vendor/bootstrap.min.js")%>'></script>
    
   
  </body>
</html>


