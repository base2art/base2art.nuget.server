﻿<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!--
          -->
          <a class="navbar-brand" href='<%= VirtualPathUtility.ToAbsolute("~/Default.aspx")%>'>NuGet Server</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li>
              <a href='<%= VirtualPathUtility.ToAbsolute("~/app/security/configure.aspx") %>'>Manage Security</a>
            </li>
            <li>
              <a href='<%= VirtualPathUtility.ToAbsolute("~/app/change-log.aspx") %>'>Change Log</a>
            </li>
            <li>
              <a href='<%= VirtualPathUtility.ToAbsolute("~/app/invalidate-package-cache") %>'>Invalidate Cache</a>
            </li>
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>