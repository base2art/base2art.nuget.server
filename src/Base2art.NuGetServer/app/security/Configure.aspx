﻿<%@ Page
	Language           = "C#"
	AutoEventWireup    = "false"
	ValidateRequest    = "false"
	EnableSessionState = "false"
%>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Authentication" %>
<%@ Import Namespace="Base2art.Security.Authorization" %>
<%@ Import Namespace="Base2art.Security.Characterization" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="nav" Src="~/app/nav-bar.ascx" %>
<%@ Register TagPrefix="b2a" TagName="head" Src="~/app/html-head.ascx" %>
<%@ Register TagPrefix="b2a" TagName="copyFooter" Src="~/app/body-copy-footer.ascx" %>


<%@ Register TagPrefix="b2a" TagName="UsersList" Src="Users/List.ascx" %>
<%@ Register TagPrefix="b2a" TagName="UsersRoles" Src="Users/Roles.ascx" %>
<%@ Register TagPrefix="b2a" TagName="UsersEdit" Src="Users/Edit.ascx" %>

<%@ Register TagPrefix="b2a" TagName="GroupsList" Src="Groups/List.ascx" %>
<%@ Register TagPrefix="b2a" TagName="GroupsEdit" Src="Groups/Edit.ascx" %>

<%@ Register TagPrefix="b2a" TagName="RolesList" Src="Roles/List.ascx" %>
<%@ Register TagPrefix="b2a" TagName="RolesEdit" Src="Roles/Edit.ascx" %>

<%@ Register TagPrefix="b2a" TagName="RolesUsersList" Src="Roles/Users.ascx" %>
<%@ Register TagPrefix="b2a" TagName="RolesGroupsList" Src="Roles/Groups.ascx" %>

<%@ Register TagPrefix="b2a" TagName="GroupsUsersList" Src="Groups/Users.ascx" %>
<%@ Register TagPrefix="b2a" TagName="GroupsGroupsList" Src="Groups/Groups.ascx" %>

<%@ Register TagPrefix="b2a" TagName="RestrictionsRoleBasedItemsList" Src="Restrictions/RoleBasedItemsList.ascx" %>
<%@ Register TagPrefix="b2a" TagName="RestrictionsRoleBasedItemsEdit" Src="Restrictions/RoleBasedItemsEdit.ascx" %>
<%@ Register TagPrefix="b2a" TagName="RestrictionsWhiteListItemsList" Src="Restrictions/WhiteListItemsList.ascx" %>
<%@ Register TagPrefix="b2a" TagName="RestrictionsWhiteListItemsEdit" Src="Restrictions/WhiteListItemsEdit.ascx" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">


        protected System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>();
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            
            var securityService = this.Context.Services().Find<ISecurityService>();
            securityService.AuthenticationProcessor.FindUserByName(string.Empty);
            
            if (securityService.AuthenticationProcessor.HasUsers())
            {
                if (securityService.CanAccess(this.Context.RequestData(), "Security:Administrator") == SecurityStatus.Success)
                {
                    this.ShowAdvancedSettings(securityService);
                }
                else
                {
                    throw new Exception("Not Allowed Access");
                }
            }
            else
            {
            }
        }
        
        protected override void OnLoad(System.EventArgs ea)
        {
            base.OnLoad(ea);
            
            var securityService = this.Context.Services().Find<ISecurityService>();
            
            if (this.Request.Form["action"] == "Setup System")
            {
                this.SetupSystem(securityService);
            }
        }
        
        private void SetupSystem(ISecurityService securityService)
        {
            // Setup User
            var userData = new UserData();
            userData.Name = this.Request.Form["__username"];
            userData.Email = this.Request.Form["__email"];
            userData.Password = this.Request.Form["__password"];
            var result1 = securityService.AuthenticationProcessor.Register(userData);
            if (result1 != RegisterStatus.Success)
            {
                this.errors.Add(string.Format("[{0}] [{1:G}]", "RegisterStatus", result1));
            }
            
            // Setup Groups
            
            // Setup Roles
            
            var securityRoleData = new RoleData();
            securityRoleData.Name = this.Request.Form["__security_admin_name"];
            securityRoleData.Description = "This role gives users access to change security settings.";
            var result2 = securityService.CharacterizationProcessor.CreateRole(securityRoleData);
            if (result2 != RoleCreationStatus.Success)
            {
                this.errors.Add(string.Format("[{0}] [{1:G}]", "RoleCreationStatus", result2));
            }
            
            var pageViewRoleData = new RoleData();
            pageViewRoleData.Name = this.Request.Form["__viewer_role_name"];
            
            if (!string.IsNullOrWhiteSpace(pageViewRoleData.Name))
            {
                pageViewRoleData.Description = "This role gives users access to view pages.";
                var result3 = securityService.CharacterizationProcessor.CreateRole(pageViewRoleData);
                if (result3 != RoleCreationStatus.Success)
                {
                    this.errors.Add(string.Format("[{0}] [{1:G}]", "RoleCreationStatus", result3));
                }
            }
            
            // Sync up roles
            var result4 = securityService.CharacterizationProcessor.AssignUserToRole(securityRoleData.Name, userData.Name);
            if (result4 != RoleAssignmentStatus.Success)
            {
                this.errors.Add(string.Format("[{0}] [{1:G}]", "RoleAssignmentStatus", result4));
            }
            
            
            if (!string.IsNullOrWhiteSpace(pageViewRoleData.Name))
            {
                var result5 = securityService.CharacterizationProcessor.AssignUserToRole(pageViewRoleData.Name, userData.Name);
                if (result5 != RoleAssignmentStatus.Success)
                {
                    this.errors.Add(string.Format("[{0}] [{1:G}]", "RoleAssignmentStatus", result5));
                }
            }
            
            
            // Setup Restrictions
            
            //  //  Security Override
            var restrictionData = new RoleBasedPermissionData();
            restrictionData.AccessLevel = AccessLevel.Allow;
            restrictionData.Roles = new string[] { securityRoleData.Name };
            restrictionData.Users = new string[0];
            restrictionData.Uri = new UriMapFilterData
            {
                Path = this.Request.Path
            };
            
            var result6 = securityService.AuthorizationProcessor.AddRoleBasedPermission(restrictionData);
            if (result6 != CreatePermissionStatus.Success)
            {
                this.errors.Add(string.Format("[{0}] [{1:G}]", "Permission", result6));
            }
            
            //  //  Security Override
            var allPagesRestrictionData = new RoleBasedPermissionData();
            allPagesRestrictionData.AccessLevel = AccessLevel.Allow;
            
            if (!string.IsNullOrWhiteSpace(pageViewRoleData.Name))
            {
                allPagesRestrictionData.Roles = new string[] { pageViewRoleData.Name };
            }
            else
            {
                allPagesRestrictionData.Roles = new string[0];
            }
            
            allPagesRestrictionData.Users = new string[0];
            allPagesRestrictionData.Uri = new UriMapFilterData();
            
            var result7 = securityService.AuthorizationProcessor.AddRoleBasedPermission(allPagesRestrictionData);
            if (result7 != CreatePermissionStatus.Success)
            {
                this.errors.Add(string.Format("[{0}] [{1:G}]", "Restriction", result7));
            }
            
            if (this.errors.Count == 0)
            {
                this.errors.Add("Successfully setup system. Reload Page to sign in.");
            }
        }

        private void ShowAdvancedSettings(ISecurityService security)
        {
            this.InitialSetupContainer.Visible = false;
            
            this.LinksContainer.Visible = true;
            
            this.UsersContainer.Visible = false;
            this.UsersContainerEdit.Visible = false;
            
            this.RolesContainer.Visible = false;
            this.RolesContainerEdit.Visible = false;
            
            this.GroupsContainer.Visible = false;
            this.GroupsContainerEdit.Visible = false;
            
            this.RolesUsersContainer.Visible = false;
            this.RolesGroupsContainer.Visible = false;
            
            this.WhiteListPermissionsContainer.Visible = false;
            this.WhiteListPermissionsContainerEdit.Visible = false;
            
            this.RoleBasedPermissionsContainer.Visible = false;
            this.RoleBasedPermissionsContainerEdit.Visible = false;
            
            if (this.Request.QueryString["tab"] == "users")
            {
                this.UsersContainer.Visible = true;
                this.UsersContainerEdit.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "roles")
            {
                this.RolesContainer.Visible = true;
                this.RolesContainerEdit.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "roles_users")
            {
                this.RolesUsersContainer.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "roles_groups")
            {
                this.RolesGroupsContainer.Visible = true;
            }
            
            
            if (this.Request.QueryString["tab"] == "groups")
            {
                this.GroupsContainer.Visible = true;
                this.GroupsContainerEdit.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "groups_users")
            {
                this.GroupsUsersContainer.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "groups_groups")
            {
                this.GroupsGroupsContainer.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "whitelist-permissions")
            {
                this.WhiteListPermissionsContainer.Visible = true;
                this.WhiteListPermissionsContainerEdit.Visible = true;
            }
            
            if (this.Request.QueryString["tab"] == "role-permissions")
            {
                this.RoleBasedPermissionsContainer.Visible = true;
                this.RoleBasedPermissionsContainerEdit.Visible = true;
            }
        }

</script>



<!DOCTYPE html>
<html lang="en">
  <b2a:head runat="server" />
  <body>
  
    
    <b2a:nav runat="server" />

          
    <div class="container">
      <div>
        <h1>&nbsp;</h1>
        <div class="row" style="padding-top:30px;">
          <div class="col-md-4">
          
            <asp:PlaceHolder runat="server" id="LinksContainer" visible="false">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">ACLs</h3>
                </div>
                <div class="panel-body">
	                <div class="horizontalList">
	                    <ul class="list-group">
	                        <li class='list-group-item'><a href="?tab=users">Users</a></li>
	                        <li class='list-group-item'><a href="?tab=groups">Groups</a></li>
	                        <li class='list-group-item'><a href="?tab=roles">Roles</a></li>
	                        <li class='list-group-item'><a href="?tab=role-permissions">Role-Based Permissions</a></li>
	                        <li class='list-group-item'><a href="?tab=whitelist-permissions">Whitelisted Permissions</a></li>
	                    </ul>
	                </div>
                </div>
              </div>
		    </asp:PlaceHolder>
		      
		    <asp:PlaceHolder runat="server" id="UsersContainerEdit" visible="false">
		      <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">User</h3>
                </div>
                <div class="panel-body">
	              <b2a:UsersEdit runat="server" id="UserEditor" />
                </div>
		      </div>
		    </asp:PlaceHolder>
		      
		    <asp:PlaceHolder runat="server" id="RolesContainerEdit" visible="false">
		      <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">Role</h3>
                </div>
                <div class="panel-body">
	              <b2a:RolesEdit runat="server" id="RoleEditor" />
                </div>
		      </div>
		    </asp:PlaceHolder>
		    
		      
		    <asp:PlaceHolder runat="server" id="GroupsContainerEdit" visible="false">
		      <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">Group</h3>
                </div>
                <div class="panel-body">
	              <b2a:GroupsEdit runat="server" id="GroupEditor" />
                </div>
		      </div>
		    </asp:PlaceHolder>
		    
		      
		    <asp:PlaceHolder runat="server" id="WhiteListPermissionsContainerEdit" visible="false">
		      <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">White List Item</h3>
                </div>
                <div class="panel-body">
	              <b2a:RestrictionsWhiteListItemsEdit runat="server" />
                </div>
		      </div>
		    </asp:PlaceHolder>
		    
		      
		    <asp:PlaceHolder runat="server" id="RoleBasedPermissionsContainerEdit" visible="false">
		      <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">Role Based Permission Item</h3>
                </div>
                <div class="panel-body">
	              <b2a:RestrictionsRoleBasedItemsEdit runat="server" />
                </div>
		      </div>
		    </asp:PlaceHolder>
		    
		    
		    
          </div>
          
          <div class='<%= LinksContainer.Visible ? "col-md-8" : "col-md-12" %>' >
          
	        <asp:PlaceHolder runat="server" id="InitialSetupContainer">
	          <h2>Initial Setup</h2>
		      
              <% foreach(var error in this.errors) { %>
                  <p style="font-weight:bold; color:Red;"> <%= error %></p>
              <% } %>
		      
		      <form method="POST">
		      	<fieldset>
		      		<legend>Restrict Site</legend>
		      		<%= CommonFunctions.Field_TB("User Name", "__username", required: true, defaultValue: "admin") %>
		      		<%= CommonFunctions.Field_TB("Email" , "__email", required: true) %>
		      		<%= CommonFunctions.Field_TB("Password" , "__password", required: true) %>
		      		<%= CommonFunctions.Field_TB("Page Viewer Role <br />(Leave empty to disable security for downloads)" , "__viewer_role_name", required: true, defaultValue: "Page:Viewer") %>
		      		<%= CommonFunctions.Field_TB("Security Administration Role" , "__security_admin_name", required: true, defaultValue: "Security:Administrator", @readonly: true) %>
		      		
		      		<div>
		      			<input type="submit" name="action" value="Setup System" />
		      		</div>
		      	</fieldset>
		      </form>
		  </asp:PlaceHolder>
	      
	      <asp:PlaceHolder runat="server" id="UsersContainer" visible="false">
	          <h2>User Setup</h2>
	          <% if (!UserEditor.IsCreating) { %>
                  <form style="display: inline;margin-bottom:20px;" action="?" method="GET">
                      <input name="tab" value="users" type="hidden" />
                      <button class='btn' style="display: inline;margin-bottom:20px;">Add New User</button>
                  </form>
	          <% } %>	        
	          <div style="margin-bottom:20px;">
	              <b2a:UsersList runat="server" />
	          </div>
	          <div style="margin-bottom:20px;">
	              <b2a:UsersRoles runat="server" />
	          </div>
	          
		  </asp:PlaceHolder>
	      <asp:PlaceHolder runat="server" id="GroupsContainer" visible="false">
	          <h2>Groups Setup</h2>
	          <% if (!GroupEditor.IsCreating) { %>
                  <form style="display: inline;margin-bottom:20px;" action="?" method="GET">
                      <input name="tab" value="groups" type="hidden" />
                      <button class='btn' style="display: inline;margin-bottom:20px;">Add New Group</button>
                  </form>
	          <% } %>	        
	          <div style="margin-bottom:20px;">
	              <b2a:GroupsList runat="server" />
	          </div>
	          
		  </asp:PlaceHolder>
	      <asp:PlaceHolder runat="server" id="RolesContainer" visible="false">
	          <h2>Roles Setup</h2>
	          <% if (!RoleEditor.IsCreating) { %>
                  <form style="display: inline;margin-bottom:20px;" action="?" method="GET">
                      <input name="tab" value="roles" type="hidden" />
                      <button class='btn' style="display: inline;margin-bottom:20px;">Add New Role</button>
                  </form>
	          <% } %>
	          <div style="margin-bottom:20px;">
	              <b2a:RolesList runat="server" />
	          </div>
		  </asp:PlaceHolder>
		  
	      <asp:PlaceHolder runat="server" id="RolesUsersContainer" visible="false">
	          <h2>Users with Role</h2>
	          <div style="margin-bottom:20px;">
	              <b2a:RolesUsersList runat="server" />
	          </div>
		  </asp:PlaceHolder>
		  
	      <asp:PlaceHolder runat="server" id="RolesGroupsContainer" visible="false">
	          <h2>Groups with Role</h2>
	          <div style="margin-bottom:20px;">
	              <b2a:RolesGroupsList runat="server" />
	          </div>
		  </asp:PlaceHolder>
		  
	      <asp:PlaceHolder runat="server" id="GroupsUsersContainer" visible="false">
	          <h2>Users in Group</h2>
	          <div style="margin-bottom:20px;">
	              <b2a:GroupsUsersList runat="server" />
	          </div>
		  </asp:PlaceHolder>
		  
	      <asp:PlaceHolder runat="server" id="GroupsGroupsContainer" visible="false">
	          <h2>Groups in Group</h2>
	          <div style="margin-bottom:20px;">
	              <b2a:GroupsGroupsList runat="server" />
	          </div>
		  </asp:PlaceHolder>
		  
	      <asp:PlaceHolder runat="server" id="RoleBasedPermissionsContainer" visible="false">
	          <h2>Role-Based Permissions Setup</h2>
	          <div class='well'>
	            <p>
	               If you have an entry having a `Path` that is ``(Empty) and `AccessLevel` is `Allow`, it will let all users in, unless otherwise restricted.
	            </p>
	            <p>
	               If you would like to fully open the site, with the excpetion of a handful of urls that you want locked down,
                    then you should always make sure that your empty-pathed entry appears last in the grid above. Simply delete and re-add it to make it last.
	            </p>
	          </div>
	          <div style="margin-bottom:40px;">
	              <b2a:RestrictionsRoleBasedItemsList runat="server" />
	          </div>
		  </asp:PlaceHolder>
	          
	      <asp:PlaceHolder runat="server" id="WhiteListPermissionsContainer" visible="false">
	          <h2>White-List Permissions Setup</h2>
	          
	          <div style="margin-bottom:40px;">
	              <b2a:RestrictionsWhiteListItemsList runat="server" />
	          </div>
		  </asp:PlaceHolder>
		</div>
      </div>
      
      <b2a:copyFooter runat='server' />

    </div> <!-- /container -->
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='<%= VirtualPathUtility.ToAbsolute("~/app/js/vendor/bootstrap.min.js")%>'></script>
    
   
  </body>
</html>


