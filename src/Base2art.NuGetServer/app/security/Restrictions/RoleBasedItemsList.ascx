﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.Authorization" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        var action = CommonFunctions.Value("action", string.Empty);
        
        if (action == "remove_role_based_item")
        {
            var permission = new UriMapFilterData();
            permission.CaseSensitive = CommonFunctions.ValueBool("__case_sensitive", false);
            permission.Host = CommonFunctions.Value("__host", string.Empty);
            permission.Port = CommonFunctions.ValueIntQ("__port", null);
            permission.Path = CommonFunctions.Value("__path", string.Empty);
            
            var result = CommonFunctions.Security.AuthorizationProcessor.RemoveRoleBasedPermission(permission);
            if (result == RemovePermissionStatus.Success_OneMatch || result == RemovePermissionStatus.Success_ManyMatches)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
        }
    }
    
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        var users = security.AuthorizationProcessor.RoleBasedPermissions(new Base2art.Collections.Specialized.Page(0, 20));
        this.CurrentItems.DataSource = users;
        this.CurrentItems.DataBind();
    }
    
    
    
    private IEnumerable<string> Users()
    {
        var security = this.Context.Services().Find<ISecurityService>();
        
        var allUsers = security.AuthenticationProcessor.Users(new Base2art.Collections.Specialized.Page(0, 10000));
        return allUsers.Select(x => x.Name);
    }
    
    
    
    private IEnumerable<string> Roles()
    {
        var security = this.Context.Services().Find<ISecurityService>();
        
        var allRoles = security.CharacterizationProcessor.Roles(new Base2art.Collections.Specialized.Page(0, 10000));
        return allRoles.Select(x => x.Name);
    }
    
    
</script>


<% foreach(var error in this.errors) { %>
    <span style="font-weight:bold; color:Red;"> <%= error %></span>
<% } %>


<table style="margin-bottom:30px;"  class="table">
    <thead>
        <td>
            Access Level
        </td>
        <td>
            Host
        </td>
        <td>
            Path
        </td>
        <td>
            Case Sensitive
        </td>
        <td>
            Port
        </td>
        <td>
            Roles
        </td>
        <td>
            Users
        </td>
        <td>
            Manage
        </td>
        
    </thead>
    <asp:Repeater runat="server" id="CurrentItems">
         <ItemTemplate>
             <tr>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AccessLevel") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Uri.Host") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Uri.Path") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Uri.CaseSensitive") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Uri.Port") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# CommonFunctions.Truncate(75, string.Join(",", (string[])DataBinder.Eval(Container.DataItem, "Roles"))) %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" Text='<%# CommonFunctions.Truncate(75, string.Join(",", (string[])DataBinder.Eval(Container.DataItem, "Users"))) %>' Mode="Encode" />
                 </td>
                 <td style="width:1px;">
                     <form method="POST" style="display: inline;">
                           <input type="hidden" name="action" value="remove_role_based_item" />
                           <input type="hidden" name="__host" value='<%# DataBinder.Eval(Container.DataItem, "Uri.Host") %>' />
                           <input type="hidden" name="__path" value='<%# DataBinder.Eval(Container.DataItem, "Uri.Path") %>' />
                           <input type="hidden" name="__port" value='<%# DataBinder.Eval(Container.DataItem, "Uri.Port") %>' />
                           <input type="hidden" name="__case_sensitive" value='<%# DataBinder.Eval(Container.DataItem, "Uri.CaseSensitive") %>' />
                           <button class='btn'> Remove </button>
                     </form>
                     
                 </td>
             </tr>
         </ItemTemplate>
    </asp:Repeater>
</table>

