﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.Authorization" %>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        var action = CommonFunctions.Value("action", string.Empty);
        
        
        if (action == "create_white_list_item")
        {
            var permission = new UriMapFilterData();
            permission.CaseSensitive = CommonFunctions.ValueBool("__case_sensitive", false);
            permission.Host = CommonFunctions.Value("__host", string.Empty);
            permission.Port = CommonFunctions.ValueIntQ("__port", null);
            permission.Path = CommonFunctions.Value("__path", string.Empty);
            
            var result = CommonFunctions.Security.AuthorizationProcessor.AddWhiteListPermission(permission);
            if (result == CreatePermissionStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
        }
    }
    
</script>


<% foreach(var error in this.errors) { %>
    <span style="font-weight:bold; color:Red;"> <%= error %></span>
<% } %>


<form method="POST">
    <fieldset>
        
        <legend>Create WhiteList Permission</legend>
        
        <%= CommonFunctions.Field_TB("Host", "__host") %>
        <%= CommonFunctions.Field_TB("Path", "__path") %>
        <%= CommonFunctions.Field_TB("Port", "__port") %>
        <%= CommonFunctions.Field_CB("Path Is Case Sensitive", "__case_sensitive") %>
        
        <div>
            <input class='btn' type="submit" name="action" value="create_white_list_item" />
            
            <% foreach(var error in this.errors) { %>
                <span style="font-weight:bold; color:Red;"> <%= error %></span>
            <% } %>
            
        </div>
    </fieldset>
</form>