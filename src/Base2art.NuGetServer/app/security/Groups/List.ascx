﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Membership" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>
<%@ Import Namespace="Base2art.Collections.Specialized" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">
    
    private IPagedCollection<IGroup> groups;
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        var security = this.Context.Services().Find<ISecurityService>();
        this.groups = security.MembershipProcessor.Groups(new Base2art.Collections.Specialized.Page(CommonFunctions.ValueInt("group-page", 0), 20));
        this.CurrentGroups.DataSource = this.groups;
        this.CurrentGroups.DataBind();
    }
    
</script>

<table class="table">
    <thead>
        <td>
            Group Name
        </td>
        <td>
            Group Description
        </td>
        <td>
            Manage
        </td>
        <td>
            Users
        </td>
        <td>
            Groups
        </td>
        
    </thead>
    <asp:Repeater runat="server" id="CurrentGroups">
         <ItemTemplate>
             <tr>
                 <td>
                     <asp:literal runat="server" id="groupName" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' Mode="Encode" />
                 </td>
                 <td>
                     <asp:literal runat="server" id="description" Text='<%# CommonFunctions.Truncate(75, DataBinder.Eval(Container.DataItem, "Description")) %>' Mode="Encode" />
                 </td>
                 <td>
                     <a href='?tab=groups&group=<%# DataBinder.Eval(Container.DataItem, "Name") %>' >Manage</a>
                 </td>
                 <td>
                     <a href='?tab=groups_users&group=<%# DataBinder.Eval(Container.DataItem, "Name") %>' > Edit (<%# DataBinder.Eval(Container.DataItem, "MemberUsers.Length") %>) </a>
                 </td>
                 <td>
                     <a href='?tab=groups_groups&group=<%# DataBinder.Eval(Container.DataItem, "Name") %>' > Edit (<%# DataBinder.Eval(Container.DataItem, "MemberGroups.Length") %>) </a>
                 </td>
             </tr>
         </ItemTemplate>
    </asp:Repeater>
</table>


<div style="float-left; padding:20px;">
    
    <% if ( this.groups.Page.HasPreviousPage()) { %> 
        <form method="GET" style="display: inline;">
            <input type="hidden" name="tab" value="groups" />
            <input type="hidden" name="group-page" value='<%= CommonFunctions.ValueInt("group-page", 0) - 1 %>' />
            <button style="margin-right:20px;"> &laquo; Previous  </button>
        </form>
    <% } %>
    
    Showing [<%= this.groups.Page.Start() + 1 %> - <%= this.groups.Page.End() + 1 %>] of <%= this.groups.Page.TotalCount %> 
    
    <% if ( this.groups.Page.HasNextPage().GetValueOrDefault()) { %> 
        <form method="GET" style="display: inline;">
            <input type="hidden" name="tab" value="groups" />
            <input type="hidden" name="group-page" value='<%= CommonFunctions.ValueInt("group-page", 0) + 1 %>' />
            <button style="margin-left:20px;"> Next &raquo; </button>
        </form>
    <% } %>
</div>