﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<script runat="server">

    public ISecurityService Security
    {
        get { return this.Context.Services().Find<ISecurityService>(); }
    }
    
    public string Value(string key, string defaultValue)
    {
        string valueQs = this.Request.QueryString[key];
        
        if (!string.IsNullOrWhiteSpace(valueQs))
        {
            return valueQs;
        }
        
        string value = this.Request.Form[key];
        
        if (!string.IsNullOrWhiteSpace(value))
        {
            return value;
        }
        
        return defaultValue;
    }
    
    public int ValueInt(string key, int defaultValue)
    {
        return ValueIntQ(key, defaultValue).Value;
    }
    
    public int? ValueIntQ(string key, int? defaultValue)
    {
        string valueQs = this.Request.QueryString[key];
        var value = 0;
        if (!string.IsNullOrWhiteSpace(valueQs))
        {
            if (int.TryParse(valueQs, out value))
            {
                return value;
            }
        }
        
        string valueForm = this.Request.Form[key];
        
        if (!string.IsNullOrWhiteSpace(valueForm))
        {
            if (int.TryParse(valueForm, out value))
            {
                return value;
            }
        }
        
        return defaultValue;
    }
    
    public bool ValueBool(string key, bool defaultValue)
    {
        return ValueBoolQ(key, defaultValue).Value;
    }
    
    public bool? ValueBoolQ(string key, bool? defaultValue)
    {
        string valueQs = this.Request.QueryString[key];
        var value = false;
        if (!string.IsNullOrWhiteSpace(valueQs))
        {
            if (bool.TryParse(valueQs, out value))
            {
                return value;
            }
            
            if (valueQs == "on")
            {
                return true;
            }
        }
        
        string valueForm = this.Request.Form[key];
        
        if (!string.IsNullOrWhiteSpace(valueForm))
        {
            if (bool.TryParse(valueForm, out value))
            {
                return value;
            }
            
            if (valueForm == "on")
            {
                return true;
            }
        }
        
        return defaultValue;
    }
    
    public string Truncate(int characterLength, object textObj)
    {
        if (textObj == null)
        {
            return string.Empty;
        }
        
        var text = textObj.ToString();
        if (text.Length > characterLength)
        {
            return text.Substring(0, characterLength - 3) + "...";
        }
        
        return text;
    }

    public string Field_TB(
        string labelText,
        string name,
        bool required = false,
        object defaultValue = null,
        bool @readonly = false)
    {
        StringBuilder sb = new StringBuilder();
        Field_Begin(sb, labelText, required);
        sb.AppendFormat(
            "<input type='text' name='{0}' value='{1}' {2}  class='form-control' />",
            name,
            Value(name, defaultValue == null ? "" : defaultValue.ToString()),
            @readonly ? "readonly='readonly'" : "");
            
        Field_End(sb);
        return sb.ToString();
    }

    public string Field_CB(
        string labelText,
        string name,
        bool @checked = false)
    {
        StringBuilder sb = new StringBuilder();
        Field_Begin(sb, labelText, false);
        sb.AppendFormat(
            "<input type='checkbox' name='{0}' {1} />",
            name,
            @checked ? "checked='checked'" : "");
            
        Field_End(sb);
        return sb.ToString();
    }

    public string Field_TextArea(
        string labelText,
        string name,
        bool required = false,
        object defaultValue = null,
        bool @readonly = false)
    {
        StringBuilder sb = new StringBuilder();
        Field_Begin(sb, labelText, required);
        sb.AppendFormat(
            "<textarea name='{0}' class='form-control' height:180px;'>{1}</textarea>",
            name,
            HttpUtility.HtmlEncode(Value(name, defaultValue == null ? "" : defaultValue.ToString())),
            @readonly ? "readonly='readonly'" : "");
            
        Field_End(sb);
        return sb.ToString();
    }
    
    public void Field_Begin(StringBuilder sb, string labelText, bool required)
    {
        sb.AppendLine("<div style='margin-bottom:15px;'>");
        
        sb.AppendLine("  <label style='width:100%'>");
        
        sb.Append("     <span style='display:block;'>");
        sb.Append(labelText);
        
        if (required)
        {
            sb.Append(" ");
            sb.Append("(*)");
        }
        
        sb.AppendLine(":</span>");
    }
    
    public void Field_End(StringBuilder sb)
    {
        sb.AppendLine();
        
        sb.AppendLine("  </label>");
        
        sb.AppendLine("</div>");
    }
    
</script>
