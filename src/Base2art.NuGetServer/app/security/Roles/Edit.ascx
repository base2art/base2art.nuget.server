﻿<%@ Control
    Language           = "C#"
%>
<%@ Import Namespace="Base2art.Security.AccessControl" %>
<%@ Import Namespace="Base2art.Security.Characterization" %>
<%@ Import Namespace="Base2art.Security.Web.Module" %>

<%@ Register TagPrefix="b2a" TagName="CommonFunctions" Src="../CommonFuncts.ascx" %>

<b2a:CommonFunctions id="CommonFunctions" runat="server" />

<script runat="server">

    private IRole role;
    private bool hasChecked;
    
    private readonly System.Collections.Generic.List<string> errors = new System.Collections.Generic.List<string>(); 
    
    public bool IsCreating
    {
        get { return string.IsNullOrWhiteSpace(this.Role.Name); }
    }
    
    private IRole Role
    {
        get
        {
            if (!this.hasChecked)
            {
                var roleName = CommonFunctions.Value("role", string.Empty);
                this.role = CommonFunctions.Security.CharacterizationProcessor.FindRoleByName(roleName);
                
                if (this.role == null)
                {
                    this.role = new Base2art.Security.Characterization.Repositories.Models.Role();
                }
                
                this.hasChecked = true;
            }
            
            return this.role;
        }
    }
    
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        var action = CommonFunctions.Value("action", string.Empty);
        
        if (action == "Update_Role")
        {
            var roleData = new RoleData(); 
            roleData.Name = CommonFunctions.Value("__name", string.Empty);
            roleData.Description = CommonFunctions.Value("__description", string.Empty);
            var result = CommonFunctions.Security.CharacterizationProcessor.UpdateRole(roleData);
            if (result == RoleCreationStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
        
        if (action == "Delete_Role")
        { 
            var roleName = CommonFunctions.Value("role", string.Empty);
            var result = CommonFunctions.Security.CharacterizationProcessor.RemoveRole(roleName);
            if (result == RoleRemovalStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
        
        if (action == "Create_Role")
        {
            var roleData = new RoleData(); 
            roleData.Name = CommonFunctions.Value("__name", string.Empty);
            roleData.Description = CommonFunctions.Value("__description", string.Empty);
            var result = CommonFunctions.Security.CharacterizationProcessor.CreateRole(roleData);
            if (result == RoleCreationStatus.Success)
            {
                this.Response.Redirect(this.Request.Url.ToString());
            }
            
            this.errors.Add(result.ToString("G"));
            this.hasChecked = false;
        }
    }
    
</script>

<form method="POST">
    <fieldset>
        
        <% if (this.IsCreating) { %>
            <legend>Create Role</legend>
        <% } else { %>
            <legend>Delete Role</legend>
        <% } %>
        
        <%= CommonFunctions.Field_TB("Name", "__name", required: true, defaultValue: this.Role.Name, @readonly: !this.IsCreating) %>
        <%= CommonFunctions.Field_TextArea("Description", "__description", required: true, defaultValue: this.Role.Description) %>
        
        
        <div>
            <% if (this.IsCreating) { %>
                <input class='btn' type="submit" name="action" value="Create_Role" />
            <% } else { %>
                <input class='btn' type="submit" name="action" value="Update_Role" />
                &nbsp;
                <input class='btn' type="submit" name="action" value="Delete_Role" /> 
            <% } %>
            
            <% foreach(var error in this.errors) { %>
                <span style="font-weight:bold; color:Red;"> <%= error %></span>
            <% } %>
            
        </div>
    </fieldset>
</form>
