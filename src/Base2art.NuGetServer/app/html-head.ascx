﻿
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <title>NuGet Repository</title>

    <!-- Bootstrap -->
    <link href='<%= VirtualPathUtility.ToAbsolute("~/app/css/vendor/bootstrap.min.css")%>' rel="stylesheet">
    <link href='<%= VirtualPathUtility.ToAbsolute("~/app/css/vendor/bootstrap-theme.min.css")%>' rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
    .carousel-indicators li {
      background-color: #d9edf7
    }
    .carousel-indicators li.active {
      background-color: #afd9ef; //c4e3f3 
    }
    
    
    </style>
  </head>
  