﻿namespace Base2art.NuGetServer.Security
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    
    public class BasicAuthSecurityModule : Component, IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += (sender, e) => HandleBeginRequest(context.Context);
        }
        
        private static void HandleBeginRequest(HttpContext context)
        {
            // TODO: ADD VDir Support
            var absolutePath = context.Request.Url.AbsolutePath;
            if (absolutePath.StartsWith("/auth!", StringComparison.OrdinalIgnoreCase))
            {
                var parts = absolutePath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                
                var newUrl = "~/" + string.Join("/", parts.Skip(1));
                
                if (newUrl.EndsWith("nuget", StringComparison.OrdinalIgnoreCase))
                {
                    newUrl += "/";
                }
                
                var token = parts[0];
                token = token.Substring(5).Trim();
                context.Items["Base2art.AuthToken"] = token;
                
                context.RewritePath(newUrl, true);
                
                var nextHeaderToCheck = string.Format(CultureInfo.InvariantCulture, "http-equiv=\"{0}\"", "Authorization");
                context.Items.Add(nextHeaderToCheck, "Basic " + token);
            }
        }
    }
}
