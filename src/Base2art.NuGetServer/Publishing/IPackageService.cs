﻿namespace Base2art.NuGetServer
{
    using System.Web;
    
    public interface IPackageService
    {
        void CreatePackage(HttpContextBase context);

        void PublishPackage(HttpContextBase context);

        void DeletePackage(HttpContextBase context);

        void DownloadPackage(HttpContextBase context);
    }
}
