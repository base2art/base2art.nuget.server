﻿namespace Base2art.NuGetServer
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Web.Routing;
    using DataServices;
    using Infrastructure;
    using NuGet;
    
    public class PackageService : IPackageService
    {
        private const string ApiKeyHeader = "X-NUGET-APIKEY";
        private readonly IServerPackageRepository serverRepository;
        private readonly IPackageAuthenticationService authenticationService;

        public PackageService(
            IServerPackageRepository repository,
            IPackageAuthenticationService authenticationService)
        {
            this.serverRepository = repository;
            this.authenticationService = authenticationService;
        }

        public void CreatePackage(HttpContextBase context)
        {
            var request = context.Request;

            // Get the api key from the header
            string apiKey = request.Headers[ApiKeyHeader];

            // Get the package from the request body
            Stream stream = request.Files.Count > 0 ? request.Files[0].InputStream : request.InputStream;

            var package = new ZipPackage(stream);

            // Make sure they can access this package
            if (this.Authenticate(context, apiKey, package.Id))
            {
                try
                {
                    this.serverRepository.AddPackage(package);
                    WriteStatus(context, HttpStatusCode.Created, string.Empty);
                }
                catch (InvalidOperationException ex)
                {
                    WriteStatus(context, HttpStatusCode.InternalServerError, ex.Message);
                }
            }
        }

        public void PublishPackage(HttpContextBase context)
        {
            // No-op
        }

        public void DeletePackage(HttpContextBase context)
        {
            RouteData routeData = GetRouteData(context);

            // Extract the apiKey, packageId and make sure the version if a valid version string
            // (fail to parse if it's not)
            string apiKey = context.Request.Headers[ApiKeyHeader];
            string packageId = routeData.GetRequiredString("packageId");
            var version = new SemanticVersion(routeData.GetRequiredString("version"));

            IPackage requestedPackage = this.serverRepository.FindPackage(packageId, version);

            if (requestedPackage == null || !requestedPackage.Listed)
            {
                // Package not found
                WritePackageNotFound(context, packageId, version);
            }
            else if (this.Authenticate(context, apiKey, packageId))
            {
                this.serverRepository.RemovePackage(packageId, version);
            }
        }

        public void DownloadPackage(HttpContextBase context)
        {
            RouteData routeData = GetRouteData(context);
            
            // Get the package file name from the route
            string packageId = routeData.GetRequiredString("packageId");
            var version = new SemanticVersion(routeData.GetRequiredString("version"));

            IPackage requestedPackage = this.serverRepository.FindPackage(packageId, version);

            if (requestedPackage != null)
            {
                Package packageMetatada = this.serverRepository.GetMetadataPackage(requestedPackage);
                context.Response.AddHeader("content-disposition", string.Format(CultureInfo.InvariantCulture, "attachment; filename={0}", packageMetatada.Path));
                context.Response.ContentType = "application/zip";
                context.Response.TransmitFile(packageMetatada.FullPath);
            }
            else
            {
                // Package not found
                WritePackageNotFound(context, packageId, version);
            }
        }

        private static void WriteForbidden(HttpContextBase context, string packageId)
        {
            WriteStatus(context, HttpStatusCode.Forbidden, string.Format(CultureInfo.InvariantCulture, "Access denied for package '{0}'.", packageId));
        }

        private static void WriteStatus(HttpContextBase context, HttpStatusCode statusCode, string body = null)
        {
            context.Response.StatusCode = (int)statusCode;
            if (!string.IsNullOrEmpty(body))
            {
                context.Response.StatusDescription = body;
            }
        }

        private static void WritePackageNotFound(HttpContextBase context, string packageId, SemanticVersion version)
        {
            WriteStatus(context, HttpStatusCode.NotFound, string.Format(CultureInfo.InvariantCulture, "'Package {0} {1}' Not found.", packageId, version));
        }

        private static RouteData GetRouteData(HttpContextBase context)
        {
            return RouteTable.Routes.GetRouteData(context);
        }

        private bool Authenticate(HttpContextBase context, string apiKey, string packageId)
        {
            if (this.authenticationService.IsAuthenticated(context.User, apiKey, packageId))
            {
                return true;
            }
            else
            {
                WriteForbidden(context, packageId);
                return false;
            }
        }
    }
}