﻿namespace Base2art.NuGetServer
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Web;

    public class RoutingModule : Component, IHttpModule
    {
        private static bool hasInited = false;
        
        public void Init(HttpApplication context)
        {
            if (!hasInited)
            {
                hasInited = true;
                NuGetRoutes.Start();
            }
        }
    }
}
