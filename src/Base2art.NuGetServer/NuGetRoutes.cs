﻿// [assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Base2art.NuGetServer.NuGetRoutes), "Start")]
namespace Base2art.NuGetServer
{
    using System;
    using System.Data.Services;
    using System.Linq;
    using System.ServiceModel.Activation;
    using System.Web;
    using System.Web.Routing;
    using Base2art.Collections.Specialized;
    using DataServices;
    using Infrastructure;
    using Ninject;
    using NuGet;
    using RouteMagic;
    
    public static class NuGetRoutes
    {
        public static void Start()
        {
            MapRoutes(RouteTable.Routes);
        }
        
        public static IPackageService CreatePackageService()
        {
            return NinjectBootstrapper.Kernel.Get<IPackageService>();
        }

        public static IServerPackageRepository CreatePackageRepository()
        {
            return NinjectBootstrapper.Kernel.Get<IServerPackageRepository>();
        }
        
        public static IPagedCollection<IPackage> Packages(
            string searchTerm,
            string orderBy,
            int pageIndex,
            int pageSize)
        {
            orderBy = string.IsNullOrWhiteSpace(orderBy) ? "id" : orderBy;
            
            var repo = CreatePackageRepository();
            var querable = repo.Search(searchTerm, null, true);
            
            if (string.Equals("id", orderBy, System.StringComparison.Ordinal))
            {
                querable = querable.OrderBy(x => x.Id);
            }
            
            if (string.Equals("version", orderBy, System.StringComparison.Ordinal))
            {
                querable = querable.OrderBy(x => x.Version);
            }
            
            if (string.Equals("date", orderBy, System.StringComparison.Ordinal))
            {
                querable = querable.OrderBy(x => x.Published);
            }
            
            var matchingCount = querable.Count();
            Page page = new Page(pageIndex, pageSize);
            
            querable = querable.Skip(page.Start()).Take(page.PageSize);
            
            return new PagedCollection<IPackage>(querable.Take(pageSize).ToArray(), page, matchingCount);
        }
        
        public static void SetupRoutes(RouteCollection routes)
        {
            // The default route is http://{root}/nuget/Packages
            var factory = new DataServiceHostFactory();
            var serviceRoute = new ServiceRoute("nuget", factory, typeof(Packages));
            serviceRoute.Defaults = new RouteValueDictionary { { "serviceType", "odata" } };
            serviceRoute.Constraints = new RouteValueDictionary { { "serviceType", "odata" } };
            routes.Add("nuget", serviceRoute);
        }

        private static void MapRoutes(RouteCollection routes)
        {
            // Route to create a new package
            routes.MapDelegate(
                "CreatePackage-Root",
                string.Empty,
                new { httpMethod = new HttpMethodConstraint("PUT") },
                context => CreatePackageService().CreatePackage(context.HttpContext));

            routes.MapDelegate(
                "CreatePackage",
                "api/v2/package",
                new { httpMethod = new HttpMethodConstraint("PUT") },
                context => CreatePackageService().CreatePackage(context.HttpContext));
            
            // Route to delete packages
            routes.MapDelegate(
                "DeletePackage-Root",
                "{packageId}/{version}",
                new { httpMethod = new HttpMethodConstraint("DELETE") },
                context => CreatePackageService().DeletePackage(context.HttpContext));
            
            routes.MapDelegate(
                "DeletePackage",
                "api/v2/package/{packageId}/{version}",
                new { httpMethod = new HttpMethodConstraint("DELETE") },
                context => CreatePackageService().DeletePackage(context.HttpContext));

            // Route to get packages
            routes.MapDelegate(
                "DownloadPackage",
                "api/v2/package/{packageId}/{version}",
                new { httpMethod = new HttpMethodConstraint("GET") },
                context => CreatePackageService().DownloadPackage(context.HttpContext));

            routes.MapDelegate(
                "InvalidatePackageCache",
                "app/invalidate-package-cache",
                new { httpMethod = new HttpMethodConstraint("GET") },
                context => 
                {
                    CreatePackageRepository().InvalidatePackageCache();
                    context.HttpContext.Response.Redirect("~/Default.aspx?message=InvalidatePackageCache:Success");
                });
            
            SetupRoutes(routes);
        }
    }
}
