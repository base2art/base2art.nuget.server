﻿<%@ Page Language="C#" %>

<%@ Import Namespace="Base2art.NuGetServer" %>
<%@ Import Namespace="Base2art.Collections.Specialized" %>

<%@ Register TagPrefix="b2a" TagName="nav" Src="~/app/nav-bar.ascx" %>
<%@ Register TagPrefix="b2a" TagName="head" Src="~/app/html-head.ascx" %>
<%@ Register TagPrefix="b2a" TagName="copyFooter" Src="~/app/body-copy-footer.ascx" %>

<script runat="server">
    
    private string searchTerm;
    private string orderBy;
    private int pageIndex;
    private int pageSize;
    private bool isLocalRequest;
    private Base2art.Collections.Specialized.IPagedCollection<NuGet.IPackage> packages;
    
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        
        this.searchTerm = this.Request.QueryString.Get("st");
        this.orderBy = this.Request.QueryString.Get("ob");
        
        this.isLocalRequest = false &&
                                this.Request.IsLocal;
        
        int.TryParse(this.Request.QueryString.Get("p"), out this.pageIndex);
        if (!int.TryParse(this.Request.QueryString.Get("ps"), out this.pageSize))
        {
            this.pageSize = 10;
        }
    }
    
    protected override void OnPreRender(System.EventArgs e)
    {
        base.OnPreRender(e);
        
        this.packages = Base2art.NuGetServer.NuGetRoutes.Packages(
                                    searchTerm,
                                    orderBy,
                                    this.pageIndex,
                                    this.pageSize);
    }
    

</script>


<!DOCTYPE html>
<html lang="en">
  <b2a:head runat="server" />
  <body>
  
    <b2a:nav runat="server" />

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="padding-bottom:0px;">
      <div class="container">
        <div class="row">
          <div class='col-md-4'>
            <h4 style='margin-top:43px;'>You are running</h4>
            <h3>Base2art.NuGetServer v<%= System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(Base2art.NuGetServer.DataServices.Package).Assembly.Location).ProductVersion %></h3>
            <p><a class="btn btn-primary btn-lg" href='<%= VirtualPathUtility.ToAbsolute("~/nuget/Packages") %>' role="button">View Package Feed&raquo;</a></p>
          </div>
          
          <div class="col-md-8">
            
            <% if (this.isLocalRequest) { %>
            
              <div class="panel panel-info" style="margin-top: 22px">
                <div class="panel-heading">
                  <h3 class="panel-title">Packages Directory</h3>
                </div>
                <div class="panel-body">
                  <% = Base2art.NuGetServer.Infrastructure.PackageUtility.PackagePhysicalPath%>
                </div>
              </div>
              
            <% } else { %>
              <div id="myCarousel" class="slide carousel" data-ride="carousel" data-interval="false">
                <!-- Indicators -->
                <ol class="carousel-indicators" style="bottom:-26px">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  
                  <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                  <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                  <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                </ol>
                
                <div class="carousel-inner" role="listbox"  style='margin-bottom:20px;'>
                  
                  <div class="item active">
                    <div class="panel panel-info" style="margin-top: 22px; width:80%; margin-left:10%">
                      <div class="panel-heading">
                        <h3 class="panel-title">What Is NuGet?</h3>
                      </div>
                      <div class="panel-body">
                        NuGet is a package management protocol for distributing and deploying .Net applications.
                        <a href='https://docs.nuget.org/'>Read more</a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="item">
                    <div class="panel panel-info" style="margin-top: 22px; width:80%; margin-left:10%">
                      <div class="panel-heading">
                        <h3 class="panel-title">What is a NuGet Server?</h3>
                      </div>
                      <div class="panel-body">
                        A NuGet Server allows you to host your own redistributable and deployable packages 
                        without exposing them whole world. It's all about securing your intellectual property!
                      </div>
                    </div>
                  </div>
                  
                  <div class="item">
                    <div class="panel panel-info" style="margin-top: 22px; width:80%; margin-left:10%">
                      <div class="panel-heading">
                        <h3 class="panel-title">Download your copy today</h3>
                      </div>
                      <div class="panel-body">
                        For more information: 
                        <br />
                        <a style='font-weight:bold' href='http://base2art.com/products/NuGet-Server' target="_blank">
                          Visit Base2art's NuGet Server Product Page
                        </a>
                      </div>
                    </div>
                  </div>
                  
                  <div class="item">
                    <div class="panel panel-info" style="margin-top: 22px; width:80%; margin-left:10%">
                      <div class="panel-heading">
                        <h3 class="panel-title">Consider making a donation!</h3>
                      </div>
                      <div class="panel-body">
                        Support open source software and help finance this project. Open source is important.
                        <br />
                        <a style='font-weight:bold' href='http://base2art.com/products/NuGet-Server' target="_blank">
                          Visit Base2art's Site to make a donation.
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <a class=" carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class=" carousel-control" href="#myCarousel" role="button" data-slide="next" style='left: auto; right: 0;'>
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            <% } %>
          </div>
        </div>
      </div>
    </div>

          
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
        
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">System Information</h3>
            </div>
            <div class="panel-body">
              <p>
                In the package manager settings, add the following URL to the list of Package Sources:
              </p>
              <blockquote>
                  <strong><%= Helpers.GetRepositoryUrl(Request.Url, Request.ApplicationPath) %></strong>
              </blockquote>
              <% if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["apiKey"])) { %>
                <p>
                  To enable pushing packages to this feed using the nuget command line tool (nuget.exe). Set the api key appSetting in web.config.
                </p>
              <% } %> 
              <% else { %>
                <p>
                  Use the command below to push packages to this feed using the nuget command line tool (nuget.exe).
                </p>
                <blockquote>
                  <strong>nuget push {package file} -s <%= Helpers.GetPushUrl(Request.Url, Request.ApplicationPath) %> {apikey}</strong>
                </blockquote>
              <% } %>
            </div>
          </div>
        
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Personal NuGet Url</h3>
            </div>
            <div class="panel-body">
              
              <p>
                In order to support seemless nuget support every user gets their own Repo URL with credentials built-in.
                Don't worry its every bit as safe as long as you use SSL!
              </p>
              
              <form method="POST" action='/app/personal-url.aspx'>
                <div class="form-group">
                  <input class='form-control' name='__username' type='input' placeholder="User Name" />
                </div>
                <div class="form-group">
                  <input class='form-control' name='__password' type='password' placeholder="Password" />
                </div>
                <button type="submit" class="btn btn-default">Generate Personal Url</button>
              </form>
              
            </div>
          </div>
        </div>
        
        
        <div class="col-md-8">
        
          <% if (this.Request.QueryString["Message"] == "InvalidatePackageCache:Success") { %>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Well done!</strong> You successfully refreshed the cache.
            </div>
          <% } %>
          
          <div class="panel panel-default">
            
            <div class="panel-body">
              <form method="GET" class='form-inline'>
                <input type="hidden" name="p" value='<%= 0 %>'           />
                <input type="hidden" name="ps" value='<%= this.pageSize %>'               />
                <input type="hidden" name="ob" value='<%= this.orderBy %>'                />
                <input class='form-control' name='st' type='search' style='width:85%' value='<%= this.searchTerm %>' placeholder="Package Name" />
                <button type="submit" class="btn btn-default">Search</button>
              </form>
            </div>
          </div>
          
          <div class="table-responsive">
            <table class="table table-striped" id="packages" >
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Description</th>
                  <th>Version</th>
                  <th>Author(s)</th>
                </tr>
              </thead>
              
              <tbody>
                
                <% foreach(var package in this.packages) { %>
                
                  <tr>
                    <td><%= package.Id %></td>
                    <td><%= package.Description %></td>
                    <td><%= package.Version %></td>
                    <td><%= string.Join(",", package.Authors) %></td>
                  </tr>
                <% } %>
                
              </tbody>
            </table>
          </div>
          <div class="row">
            <div class="col-md-1 col-xs-3">
              <% if (this.packages.Page.HasPreviousPage()) { %>
                <form method="GET">
                  <input type="hidden" name="p" value='<%= this.pageIndex - 1 %>'           />
                  <input type="hidden" name="ps" value='<%= this.pageSize %>'               />
                  <input type="hidden" name="st" value='<%= this.searchTerm %>'             />
                  <input type="hidden" name="ob" value='<%= this.orderBy %>'                />
                  <input class='btn btn-xs btn-info' type='submit' value='&laquo; Previous' />
                </form>
              <% } %>
            </div>
            <div class="col-md-10 col-xs-6">
            &nbsp;
            </div>
            <div class="col-md-1 col-xs-3">
              <% if (this.packages.Page.HasNextPage().GetValueOrDefault(true)) { %>
                <form method="GET">
                  <input type="hidden" name="p" value='<%= this.pageIndex + 1 %>'       />
                  <input type="hidden" name="ps" value='<%= this.pageSize %>'           />
                  <input type="hidden" name="st" value='<%= this.searchTerm %>'         />
                  <input type="hidden" name="ob" value='<%= this.orderBy %>'            />
                  <input class='btn btn-xs btn-info' type='submit' value='Next &raquo;' />
                </form>
              <% } %>
            </div>
          </div>
        </div>
      </div>

      <b2a:copyFooter runat='server' />
    </div> <!-- /container -->
  
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src='<%= VirtualPathUtility.ToAbsolute("~/app/js/vendor/bootstrap.min.js")%>'></script>
    
   
  </body>
</html>


