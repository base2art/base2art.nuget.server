﻿namespace Base2art.NuGetServer.DataServices
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    internal class QueryTranslator<T> : IOrderedQueryable<T>
    {
        private readonly Expression expression;
        private readonly QueryTranslatorProvider<T> provider;

        public QueryTranslator(IQueryable source, IEnumerable<ExpressionVisitor> visitors)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (visitors == null)
            {
                throw new ArgumentNullException("visitors");
            }

            this.expression = Expression.Constant(this);
            this.provider = new QueryTranslatorProvider<T>(source, visitors);
        }

        public QueryTranslator(IQueryable source, Expression expression, IEnumerable<ExpressionVisitor> visitors)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            
            this.expression = expression;
            this.provider = new QueryTranslatorProvider<T>(source, visitors);
        }

        public Type ElementType
        {
            get { return typeof(T); }
        }

        public Expression Expression
        {
            get { return this.expression; }
        }

        public IQueryProvider Provider
        {
            get { return this.provider; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)this.provider.ExecuteEnumerable(this.expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.provider.ExecuteEnumerable(this.expression).GetEnumerator();
        }
    }
}
