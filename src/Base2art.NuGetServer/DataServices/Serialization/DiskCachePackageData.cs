﻿namespace Base2art.NuGetServer.DataServices
{
    using Base2art.NuGetServer.Infrastructure;
    
    public class DiskCachePackageData
    {
        public SerializablePackage[] Packages { get; set; }
        
        public DerivedPackageData[] DerivedPackages { get; set; }
    }
}
