﻿namespace Base2art.NuGetServer.DataServices
{
    using System.Linq;
    using NuGet;
    
    public class SerializablePackage : IPackage
    {
        public SerializablePackage()
        {
        }
        
        public SerializablePackage(IPackage backingPackage)
        {
            this.IsAbsoluteLatestVersion = backingPackage.IsAbsoluteLatestVersion;
            this.IsLatestVersion = backingPackage.IsLatestVersion;
            this.Listed = backingPackage.Listed;
            this.Published = backingPackage.Published;
            this.ReportAbuseUrl = backingPackage.ReportAbuseUrl;
            this.DownloadCount = backingPackage.DownloadCount;
            this.Title = backingPackage.Title;
            this.IconUrl = backingPackage.IconUrl;
            this.LicenseUrl = backingPackage.LicenseUrl;
            this.ProjectUrl = backingPackage.ProjectUrl;
            this.RequireLicenseAcceptance = backingPackage.RequireLicenseAcceptance;
            this.DevelopmentDependency = backingPackage.DevelopmentDependency;
            this.Description = backingPackage.Description;
            this.Summary = backingPackage.Summary;
            this.ReleaseNotes = backingPackage.ReleaseNotes;
            this.Language = backingPackage.Language;
            this.Tags = backingPackage.Tags;
            this.Copyright = backingPackage.Copyright;
            this.MinClientVersion = backingPackage.MinClientVersion;
            this.Id = backingPackage.Id;
            this.Version = backingPackage.Version;
            this.Id = backingPackage.Id;
            this.FrameworkAssemblies = (backingPackage.FrameworkAssemblies ?? new FrameworkAssemblyReference[0]).ToList();
            this.PackageAssemblyReferences = (backingPackage.PackageAssemblyReferences ?? new PackageReferenceSet[0]).ToList();
            this.DependencySets = (backingPackage.DependencySets ?? new PackageDependencySet[0]).ToList();
            this.Authors = (backingPackage.Authors ?? new string[0]).ToList();
            this.Owners = (backingPackage.Owners ?? new string[0]).ToList();
            this.AssemblyReferences = (backingPackage.AssemblyReferences ?? new PhysicalPackageAssemblyReference[0]).Cast<PhysicalPackageAssemblyReference>().ToList();
        }
        
        public bool IsAbsoluteLatestVersion { get; set; }
        
        public bool IsLatestVersion { get; set; }
        
        public bool Listed { get; set; }
        
        public System.DateTimeOffset? Published { get; set; }
        
        public System.Uri ReportAbuseUrl { get; set; }
        
        public int DownloadCount { get; set; }
        
        public string Title { get; set; }
        
        public System.Uri IconUrl { get; set; }
        
        public System.Uri LicenseUrl { get; set; }
        
        public System.Uri ProjectUrl { get; set; }
        
        public bool RequireLicenseAcceptance { get; set; }
        
        public bool DevelopmentDependency { get; set; }
        
        public string Description { get; set; }
        
        public string Summary { get; set; }
        
        public string ReleaseNotes { get; set; }
        
        public string Language { get; set; }
        
        public string Tags { get; set; }
        
        public string Copyright { get; set; }
        
        public System.Version MinClientVersion { get; set; }
        
        public string Id { get; set; }
        
        public SemanticVersion Version { get; set; }
        
        public System.Collections.Generic.List<FrameworkAssemblyReference> FrameworkAssemblies { get; set; }
        
        public System.Collections.Generic.List<PackageReferenceSet> PackageAssemblyReferences { get; set; }
        
        public System.Collections.Generic.List<PackageDependencySet> DependencySets { get; set; }
        
        public System.Collections.Generic.List<string> Authors { get; set; }
        
        public System.Collections.Generic.List<string> Owners { get; set; }
        
        public System.Collections.Generic.List<PhysicalPackageAssemblyReference> AssemblyReferences { get; set; }
        
        System.Collections.Generic.IEnumerable<IPackageAssemblyReference> IPackage.AssemblyReferences
        {
            get { return this.AssemblyReferences; }
        }
        
        System.Collections.Generic.IEnumerable<string> IPackageMetadata.Authors
        {
            get { return this.Authors; }
        }
        
        System.Collections.Generic.IEnumerable<string> IPackageMetadata.Owners
        {
            get { return this.Owners; }
        }
        
        System.Collections.Generic.IEnumerable<FrameworkAssemblyReference> IPackageMetadata.FrameworkAssemblies
        {
            get { return this.FrameworkAssemblies; }
        }
        
        System.Collections.Generic.ICollection<PackageReferenceSet> IPackageMetadata.PackageAssemblyReferences
        {
            get { return this.PackageAssemblyReferences; }
        }
        
        System.Collections.Generic.IEnumerable<PackageDependencySet> IPackageMetadata.DependencySets
        {
            get { return this.DependencySets; }
        }
        
        public System.Collections.Generic.IEnumerable<IPackageFile> GetFiles()
        {
            throw new System.NotImplementedException();
        }
        
        public System.Collections.Generic.IEnumerable<System.Runtime.Versioning.FrameworkName> GetSupportedFrameworks()
        {
            throw new System.NotImplementedException();
        }
        
        public System.IO.Stream GetStream()
        {
            throw new System.NotImplementedException();
        }
        
        public void ExtractContents(IFileSystem fileSystem, string extractPath)
        {
            throw new System.NotImplementedException();
        }
    }
}
