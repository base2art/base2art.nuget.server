namespace Base2art.NuGetServer.DataServices
{
    using System.Linq;
    using Infrastructure;

    public class PackageContext
    {
        private readonly IServerPackageRepository repository;
        
        public PackageContext(IServerPackageRepository repository)
        {
            this.repository = repository;
        }

        public IQueryable<Package> Packages
        {
            get
            {
                return this.repository.GetPackages()
                            .Select(this.repository.GetMetadataPackage)
                            .Where(p => p != null)
                            .AsQueryable()
                            .InterceptWith(new PackageIdComparisonVisitor());
            }
        }
    }
}
