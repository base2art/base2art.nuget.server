namespace Base2art.NuGetServer.DataServices
{
    using System;
    using System.Collections.Generic;
    using System.Data.Services.Common;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.Versioning;
    using Infrastructure;
    using NuGet;

    [DataServiceKey("Id", "Version")]
    [EntityPropertyMapping("Id", SyndicationItemProperty.Title, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    [EntityPropertyMapping("Authors", SyndicationItemProperty.AuthorName, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    [EntityPropertyMapping("LastUpdated", SyndicationItemProperty.Updated, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    [EntityPropertyMapping("Summary", SyndicationItemProperty.Summary, SyndicationTextContentKind.Plaintext, keepInContent: false)]
    [HasStream]
    public class Package
    {
        public Package(IPackage package, DerivedPackageData derivedData)
        {
            this.Id = package.Id;
            this.Version = package.Version.ToString();
            this.IsPrerelease = !string.IsNullOrEmpty(package.Version.SpecialVersion);
            this.Title = package.Title;
            this.Authors = string.Join(",", package.Authors);
            this.Owners = string.Join(",", package.Owners);
            
            if (package.IconUrl != null)
            {
                this.IconUrl = package.IconUrl.GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped);
            }
            
            if (package.LicenseUrl != null)
            {
                this.LicenseUrl = package.LicenseUrl.GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped);
            }
            
            if (package.ProjectUrl != null)
            {
                this.ProjectUrl = package.ProjectUrl.GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped);
            }
            
            this.RequireLicenseAcceptance = package.RequireLicenseAcceptance;
            this.DevelopmentDependency = package.DevelopmentDependency;
            this.Description = package.Description;
            this.Summary = package.Summary;
            this.ReleaseNotes = package.ReleaseNotes;
            this.Tags = package.Tags;
            this.Dependencies = string.Join("|", package.DependencySets.SelectMany(this.ConvertDependencySetToStrings));
            this.PackageHash = derivedData.PackageHash;
            this.PackageHashAlgorithm = "SHA512";
            this.PackageSize = derivedData.PackageSize;
            this.LastUpdated = derivedData.LastUpdated.UtcDateTime;
            this.Published = derivedData.Created.UtcDateTime;
            this.Path = derivedData.Path;
            this.FullPath = derivedData.FullPath;
            this.MinClientVersion = package.MinClientVersion == null ? null : package.MinClientVersion.ToString();
            this.Listed = package.Listed;
            this.Language = package.Language;

            // set the latest flags based on the derived data
            this.IsAbsoluteLatestVersion = derivedData.IsAbsoluteLatestVersion;
            this.IsLatestVersion = derivedData.IsLatestVersion;
        }

        public string Id { get; set; }

        public string Version { get; set; }

        public bool IsPrerelease { get; private set; }

        public string Title { get; set; }
        
        public string Authors { get; set; }

        public string Owners { get; set; }

        public string IconUrl { get; set; }

        public string LicenseUrl { get; set; }

        public string ProjectUrl { get; set; }

        public int DownloadCount { get; set; }

        public bool RequireLicenseAcceptance { get; set; }

        public bool DevelopmentDependency { get; set; }

        public string Description { get; set; }

        public string Summary { get; set; }

        public string ReleaseNotes { get; set; }

        public DateTime Published { get; set; }

        public DateTime LastUpdated { get; set; }

        public string Dependencies { get; set; }

        public string PackageHash { get; set; }

        public string PackageHashAlgorithm { get; set; }

        public long PackageSize { get; set; }

        public string Copyright { get; set; }

        public string Tags { get; set; }

        public bool IsAbsoluteLatestVersion { get; set; }

        public bool IsLatestVersion { get; set; }

        public bool Listed { get; set; }

        public int VersionDownloadCount { get; set; }

        public string MinClientVersion { get; set; }

        public string Language { get; set; }
        
        internal string FullPath { get; set; }

        internal string Path { get; set; }

        private static string ConvertDependency(PackageDependency packageDependency, FrameworkName targetFramework)
        {
            if (targetFramework == null)
            {
                if (packageDependency.VersionSpec == null)
                {
                    return packageDependency.Id;
                }
                else
                {
                    return string.Format(CultureInfo.InvariantCulture, "{0}:{1}", packageDependency.Id, packageDependency.VersionSpec);
                }
            }
            else
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}:{1}:{2}", packageDependency.Id, packageDependency.VersionSpec, VersionUtility.GetShortFrameworkName(targetFramework));
            }
        }

        private IEnumerable<string> ConvertDependencySetToStrings(PackageDependencySet dependencySet)
        {
            if (dependencySet.Dependencies.Count == 0)
            {
                if (dependencySet.TargetFramework != null)
                {
                    // if this Dependency set is empty, we still need to send down one string of the form "::<target framework>",
                    // so that the client can reconstruct an empty group.
                    return new string[] { string.Format(CultureInfo.InvariantCulture, "::{0}", VersionUtility.GetShortFrameworkName(dependencySet.TargetFramework)) };
                }
            }
            else
            {
                return dependencySet.Dependencies.Select(dependency => ConvertDependency(dependency, dependencySet.TargetFramework));
            }

            return new string[0];
        }
    }
}